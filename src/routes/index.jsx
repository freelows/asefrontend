import React, { Suspense } from 'react'

import PropTypes from 'prop-types'
import { Switch, Route, Redirect } from 'react-router-dom'
import { NotFound } from 'pages'
import { EntityType } from 'models'

const renderComponent = {
  [EntityType.Financiero]: React.lazy(() => import('pages/Financieros')),
  [EntityType.Solicitud]: React.lazy(() => import('pages/Solicitudes')),
  [EntityType.Documento]: React.lazy(() => import('pages/Documentos')),
  [EntityType.Inspeccione]: React.lazy(() => import('pages/Inspecciones')),
}

const Routes = ({ entity, setEntity, clearAll }) => {
  const render = (routeEntity) => () => {
    if (entity !== routeEntity) {
      clearAll()
      setEntity(routeEntity)
    }

    const Component = renderComponent[routeEntity]

    return (
      <Suspense fallback={<div />}>
        <Component />
      </Suspense>
    )
  }

  return (
    <Switch>
      <Route path="/Financieros" render={render(EntityType.Financiero)} />
      <Route path="/Inspecciones" render={render(EntityType.Inspeccione)} />
      <Redirect exact from="/" to="/Financieros" />
      <Route component={NotFound} />
    </Switch>
  )
}

Routes.defaultProps = {
  entity: null,
}

Routes.propTypes = {
  entity: PropTypes.string,
  setEntity: PropTypes.func.isRequired,
  clearAll: PropTypes.func.isRequired,
}

export default Routes
