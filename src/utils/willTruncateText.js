const aproximateCharPixelSize = 8

const willTruncateText = (text, maxWidth) => {
  const calculation = text.length * aproximateCharPixelSize

  return calculation > maxWidth
}

export default willTruncateText
