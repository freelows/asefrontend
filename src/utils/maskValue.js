const { TableCellType } = require('models')

const maskValue = (value, componentType) => {
  switch (componentType) {
    case TableCellType.Phone:
      return '(999) 999-999'
    case TableCellType.SingleText:
      return '***********'
    default:
      return value
  }
}

export default maskValue
