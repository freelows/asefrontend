const filterByPropertyString = ({ searchBy, results, searchCriteria }) => {
  return searchCriteria && searchBy
    ? results.filter((x) => x[searchBy].includes(searchCriteria))
    : results
}

export default filterByPropertyString
