export const isNumber = (value) => {
  return typeof value === 'number'
}

export const isBoolean = (value) => {
  return typeof value === 'boolean'
}
