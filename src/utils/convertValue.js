const { TableCellType } = require('models')

const convertValue = (value, componentType) => {
  switch (componentType) {
    case TableCellType.Date:
      return !isNaN(Date.parse(value)) ? new Date(value) : ''
    default:
      return value
  }
}

export default convertValue
