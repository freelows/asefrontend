const formatPhoneNumber = (phone) => {
  const matchedPhone = phone && phone.match(/^(\d{3})(\d{3})(\d{4})$/)

  return matchedPhone
    ? `(${matchedPhone[1]}) ${matchedPhone[2]}-${matchedPhone[3]}`
    : phone
}

export default formatPhoneNumber
