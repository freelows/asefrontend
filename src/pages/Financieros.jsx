import React from 'react'
import { FinancierosContainer } from 'components/Financieros'

const Financieros = () => <FinancierosContainer />

export default Financieros
