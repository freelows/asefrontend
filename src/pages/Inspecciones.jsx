import React from 'react'
import { InspeccionesContainer } from 'components/Inspecciones'

const Inspecciones = () => <InspeccionesContainer />

export default Inspecciones
