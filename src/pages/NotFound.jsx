import React from 'react'
import Error from './Error'

const NotFound = () => (
  <Error
    buttonText="Go Home"
    description="Error! The page you are looking for was not found!"
    title="Page Not Found"
  />
)

export default NotFound
