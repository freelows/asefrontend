const EntityType = {
  Financiero: 'Financiero',
  Solicitud: 'Solicitud',
  Seguimiento: 'Seguimiento',
  Documento: 'Documento',
  Inspeccione: 'Inspeccione',
}

export default EntityType
