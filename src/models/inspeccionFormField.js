const InspeccionFormField = {
  Nombres: 'mombres',
  Apellidos: 'apellidos',
  Rut: 'rut',
  Marca: 'marca',
  Modelo: 'modelo',
  Patente: 'patente',
  Anio: 'anio',
  UbicacionVehiculo: 'ubicacionVehiculo',
}

export default InspeccionFormField
