class Inspeccione {
  constructor(inspeccione) {
    this.id = inspeccione.id
    this.rut = inspeccione.rut
    this.cliente = inspeccione.cliente
  }
}

export default Inspeccione
