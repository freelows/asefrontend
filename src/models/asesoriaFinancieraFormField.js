const AsesoriaFinancieraFormField = {
  Nombres: 'nombres',
  Apellidos: 'apellidos',
  Rut: 'rut',
  MontoAuto: 'montoAuto',
  Pie: 'pie',
  IngresoLiquido: 'ingresoLiquido',
  Plazo: 'plazo',
  BancoFinanciamientio: 'bancoFinanciamientio',
}

export default AsesoriaFinancieraFormField
