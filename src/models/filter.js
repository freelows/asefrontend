import { v1 as uuidv1 } from 'uuid'

function buildDisplayLabel(dictionary, template) {
  if (!template) return 'No template available.'

  let filterDisplayLabel = template

  for (const key in dictionary) {
    if (dictionary[key])
      filterDisplayLabel = filterDisplayLabel.replace(
        `[${key}]`,
        dictionary[key]
      )
  }

  return filterDisplayLabel
}

class Filter {
  constructor(rawFilter) {
    if (!rawFilter) return

    this.id = uuidv1()
    this.field = rawFilter.field
    this.operator = rawFilter.operator
    this.customField = rawFilter.customField
    this.value = rawFilter.value
    this.value2 = rawFilter.value2
    this.values = rawFilter.values
  }

  get displayLabel() {
    return buildDisplayLabel(
      {
        label: this.field.path,
        customField: this.customField,
        value: this.value,
        value2: this.value2,
        values:
          this.values && this.values.length
            ? this.values.map((item) => item.label || item)
            : null,
      },
      this.operator.displayLabel
    )
  }
}

export default Filter
