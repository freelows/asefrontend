class Financiero {
  constructor(financiero) {
    this.id = financiero.id
    this.rut = financiero.rut
    this.cliente = financiero.cliente
  }
}

export default Financiero
