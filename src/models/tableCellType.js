const TableCellType = {
  None: 'none',
  SingleText: 'singleText',
  MultipleLine: 'multipleLine',
  Phone: 'phone',
  Date: 'date',
  Chip: 'chip',
  Action: 'action',
  Numeric: 'numeric',
}

export default TableCellType
