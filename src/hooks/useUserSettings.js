import { useState, useEffect } from 'react'
import {
  getKey,
  setKey,
  userSettingsKey,
  isLocalStorageAvailable,
} from 'services/localStorage'

import { isBoolean } from 'utils/dataTypes'

const {
  lightTheme: localLightTheme,
  miniNavBar: localMiniNavBar,
  miniSideBar: localMiniSideBar,
} = !isLocalStorageAvailable ? {} : getKey(userSettingsKey) || {}

export default (initialState) => {
  const [lightTheme, setLightTheme] = useState(
    isBoolean(localLightTheme) ? localLightTheme : initialState.lightTheme
  )
  const [miniNavBar, setMiniNavBar] = useState(
    isBoolean(localMiniNavBar) ? localMiniNavBar : initialState.miniNavBar
  )
  const [miniSideBar, setMiniSideBar] = useState(
    isBoolean(localMiniSideBar) ? localMiniSideBar : initialState.miniNavBar
  )
  const [compania, setCompania] = useState(2)

  const toggleThemeMode = () =>
    setLightTheme((previousThemeMode) => !previousThemeMode)

  const toggleNavBarMode = () =>
    setMiniNavBar((previousMiniNavBar) => !previousMiniNavBar)

  const toggleSideBarMode = () =>
    setMiniSideBar((previousMiniSideBar) => !previousMiniSideBar)

  useEffect(() => {
    isLocalStorageAvailable &&
      setKey(userSettingsKey, { lightTheme, miniNavBar, miniSideBar, compania })
  }, [lightTheme, miniNavBar, miniSideBar, compania])

  return [
    { lightTheme, miniNavBar, miniSideBar, compania },
    { toggleThemeMode, toggleNavBarMode, toggleSideBarMode, setCompania },
  ]
}
