import { useState, useEffect } from 'react'
import { isBoolean } from 'util'

export default ({
  initialState = {},
  onSubmit,
  additionalValidations = () => {},
}) => {
  const [values, setValues] = useState(initialState)
  const [errors, setErrors] = useState({})
  const [hasErrors, setHasErrors] = useState(false)

  const onAdditionalValidation = () => {
    const result = additionalValidations(values)
    if (result && result.errors) {
      setErrors((previousErrors) => ({
        ...previousErrors,
        ...result.errors,
      }))
    }

    if (result && result.clearErrors) {
      result.clearErrors.forEach((name) => {
        clearError(name)
      })
    }
  }

  //TODO: For now will only validate if field is required ALWAYS
  //      in the future it should validate from an array of validations
  const validate = ({ name, value }) => {
    if (!value || (Array.isArray(value) && !value.length)) {
      setErrors((previousErrors) => ({
        ...previousErrors,
        [name]: 'Is required',
      }))

      return
    }

    clearError(name)
  }

  const resetForm = () => {
    setValues(initialState)
    setErrors({})
  }

  const handleSubmit = (event) => {
    event && event.preventDefault()
    !hasErrors && onSubmit(values)

    resetForm()
  }

  const handleChange = (event) => {
    if (!event) return

    const { target } = event

    target.type !== 'checkbox' && validate(target)

    setValues((previousValues) => ({
      ...previousValues,
      [target.name]: target.type === 'checkbox' ? target.checked : target.value,
    }))

    onAdditionalValidation()
  }

  const resetValue = ({ fieldName, shouldValidate = false }) => {
    const value = isBoolean(values[fieldName])
      ? false
      : Array.isArray(values[fieldName])
      ? []
      : ''

    setValues((previousValues) => ({
      ...previousValues,
      [fieldName]: value,
    }))

    if (shouldValidate) validate({ name: fieldName, value })
    onAdditionalValidation()
  }

  const clearError = (fieldName) => {
    setErrors((previousErrors) => {
      const { [fieldName]: _, ...restOfPreviousErrors } = previousErrors
      return restOfPreviousErrors ? { ...restOfPreviousErrors } : {}
    })
  }

  useEffect(() => {
    setHasErrors(Object.keys(errors).length > 0)
  }, [errors])

  return {
    values,
    errors,
    hasErrors,
    handleChange,
    handleSubmit,
    resetValue,
    resetForm,
    clearError,
  }
}
