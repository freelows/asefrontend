import React from 'react'
import ReactDOM from 'react-dom'

import { AppContainer } from './components/App'

const authContextMock = {
  _user: { profile: { name: 'Juan Perez' }, userName: 'foo@foo.com' },
  logOut: () => {},
}

const userRoclMock = {
  none: 'test',
}

function buildApp() {
  const store = require('store').default
  const Provider = require('react-redux').Provider

  return (
    <Provider store={store}>
      <AppContainer authContext={authContextMock} userRole={userRoclMock} />
    </Provider>
  )
}
ReactDOM.render(buildApp(), document.getElementById('root'))
