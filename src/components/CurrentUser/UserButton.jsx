import React, { useContext } from 'react'
import PropTypes from 'prop-types'

import { Tooltip } from 'components/ui'
import {
  Avatar,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
} from '@material-ui/core'
import AuthenticationContext from 'context/auth'

const UserButton = ({ menuId, onClick }) => {
  const {
    _user: {
      profile: { name },
    },
  } = useContext(AuthenticationContext)

  return (
    <List aria-label="User Menu">
      <Tooltip title="User Menu">
        <ListItem
          button
          aria-controls={menuId}
          aria-haspopup="true"
          aria-label="logged user"
          onClick={onClick}
        >
          <ListItemAvatar>
            <Avatar
              alt={name}
              src="https://randomuser.me/api/portraits/men/86.jpg"
            />
          </ListItemAvatar>
          <ListItemText primary={name} />
        </ListItem>
      </Tooltip>
    </List>
  )
}

UserButton.propTypes = {
  menuId: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default React.memo(UserButton)
