import React, { useContext } from 'react'

import { Menu, ListItemIcon, Typography } from '@material-ui/core'
import { ExitToApp as ExitToAppIcon } from '@material-ui/icons'

import AuthenticationContext from 'context/auth'
import { StyledMenuItem } from './styles'

const UserMenu = (muiMenuProps) => {
  const { logOut } = useContext(AuthenticationContext)
  const handleLogOut = () => logOut()

  return (
    <Menu
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      getContentAnchorEl={null}
      open={!!muiMenuProps.anchorEl}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      {...muiMenuProps}
    >
      <StyledMenuItem onClick={handleLogOut}>
        <ListItemIcon>
          <ExitToAppIcon />
        </ListItemIcon>
        <Typography variant="inherit">Log out</Typography>
      </StyledMenuItem>
    </Menu>
  )
}

export default React.memo(UserMenu)
