import React, { useState, useCallback } from 'react'

import UserMenu from './UserMenu'
import UserButton from './UserButton'

import { CurrentUserWrapper } from './styles'

const CurrentUser = () => {
  const menuId = 'user-menu'

  const [anchorEl, setAnchorEl] = useState(null)
  const handleCloseMenu = useCallback(() => setAnchorEl(null), [])
  const handleOpenMenu = useCallback(
    (event) => setAnchorEl(event.currentTarget),
    []
  )

  return (
    <CurrentUserWrapper>
      <UserButton menuId={menuId} onClick={handleOpenMenu} />
      <UserMenu anchorEl={anchorEl} id={menuId} onClose={handleCloseMenu} />
    </CurrentUserWrapper>
  )
}

export default CurrentUser
