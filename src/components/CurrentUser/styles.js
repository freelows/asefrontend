import styled from 'styled-components'
import { MenuItem as MuiMenuItem } from '@material-ui/core'

export const CurrentUserWrapper = styled.div`
  margin-left: 6px;
  min-width: 170px;

  .MuiListItem-button {
    border-radius: 4px;
  }
`
export const StyledMenuItem = styled(MuiMenuItem)`
  padding: 6px 16px 6px;
`
