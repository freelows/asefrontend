import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { ui } from 'store/modules'
import App from './App'

const { getEntity } = ui.selectors
const { userLogged, setEntity, clearAll, setUserRole } = ui.actions

const mapStateToProps = (state) => ({
  entity: getEntity(state),
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      onUserLoggedIn: userLogged,
      setEntity,
      setUserRole,
      clearPreviousEntityState: clearAll,
    },
    dispatch
  )

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App)

export default AppContainer
