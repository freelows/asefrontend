import React from 'react'
import PropTypes from 'prop-types'

import { BrowserRouter as Router } from 'react-router-dom'

import Routes from 'routes/index'
import { UserSettingsState } from 'context/userSettings'

import { TheTopBar } from 'components/layout'
import { TheNavBarContainer } from 'components/layout/TheNavBar'

import { AppThemeProvider } from 'components/ui'

import AuthenticationContext from 'context/auth'

import { AlertsContainer } from 'components'
import { Main, AppWrapper, TopBarSpace, Container } from './styles'

function App({
  authContext,
  onUserLoggedIn,
  entity,
  setEntity,
  setUserRole,
  clearPreviousEntityState,
}) {
  React.useEffect(() => {
    onUserLoggedIn({ userName: 'jose calderon' })
    setUserRole({
      piiMask: false,
      actions: {
        lead: {
          viewList: true,
          filterData: true,
          viewDetail: true,
          createFavorite: true,
          loadFavorite: true,
          listFavorites: true,
          shouldDistribute: true,
          shouldUpdate: true,
          shouldExport: true,
        },
        job: {
          viewList: true,
          viewDetail: true,
          stopJob: true,
        },
        schedule: {
          viewList: true,
          viewDetail: true,
          addSchedule: true,
          activateSwitch: true,
        },
      },
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <AuthenticationContext.Provider value={authContext}>
      <UserSettingsState>
        <AppThemeProvider>
          <AppWrapper>
            <Router>
              <TheTopBar />
              <TheNavBarContainer />
              <Main>
                <TopBarSpace />
                <Container>
                  <Routes
                    clearAll={clearPreviousEntityState}
                    entity={entity}
                    setEntity={setEntity}
                  />
                  <AlertsContainer />
                </Container>
              </Main>
            </Router>
          </AppWrapper>
        </AppThemeProvider>
      </UserSettingsState>
    </AuthenticationContext.Provider>
  )
}

App.propTypes = {
  authContext: PropTypes.instanceOf(Object).isRequired,
  clearPreviousEntityState: PropTypes.func.isRequired,
  entity: PropTypes.string,
  setEntity: PropTypes.func.isRequired,
  setUserRole: PropTypes.func.isRequired,
  onUserLoggedIn: PropTypes.func.isRequired,
}

export default App
