import React, { useEffect, useState, useContext } from 'react'
import PropTypes from 'prop-types'
import UserSettingsContext from 'context/userSettings'
import { AlertsType } from 'models'

import { EntityLayout } from '../layout'

import Header from './Header'
import Toolbar from './Toolbar'
import { ResultsContainer } from './Results'
import PaginationContainer from './Pagination'
import ScheduleForm from './Form'
import { financierosRepository } from 'api'
const Schedules = ({
  searchCriteria,
  onSearch,
  entity,
  loadConfiguration,
  showAlert,
  userRole,
}) => {
  useEffect(() => {
    loadConfiguration()
  }, [])

  const userSettingsContext = useContext(UserSettingsContext)

  const [openDialog, setOpenDialog] = useState(false)
  const [schedule, setSchedule] = useState(null)
  const [isDialogLoading, setIsDialogLoading] = useState(false)
  const [dialogTitle, setDialogTitle] = useState(null)

  const {
    viewList,
    viewDetail,
    addSchedule,
    activateSwitch,
  } = userRole.actions.schedule

  function onCloseDialog() {
    setIsDialogLoading(false)
    setOpenDialog(false)
    setSchedule(null)
    setDialogTitle(null)
  }

  function onOpenDialog() {
    setOpenDialog(true)
  }

  const onHandleOpenDialog = () => {
    onOpenDialog()
    setDialogTitle('Nuevo asesoramiento financiero')
  }

  async function onSchedule(scheduled) {
    setIsDialogLoading(true)
    let alertPayload = {
      message: '',
      alertType: AlertsType.Success,
    }

    scheduled.distributeResults =
      scheduled.distributeResults === '' ? 'All' : scheduled.distributeResults
    try {
      await financierosRepository.save(scheduled)
      await loadConfiguration()
      alertPayload = {
        ...alertPayload,
        message: `La solicitud ha sido creada con éxito`,
      }
    } catch (err) {
      alertPayload = {
        alertType: AlertsType.Error,
        message: `Error: ${err}`,
      }
    }
    onCloseDialog()
    showAlert(alertPayload)
  }

  async function onUpdate(scheduled) {
    try {
      setIsDialogLoading(true)

      await loadConfiguration()
      const alertPayload = {
        alertType: AlertsType.Success,
        message: `Actualizado.`,
      }
      showAlert(alertPayload)
    } catch (err) {
      const alertPayload = {
        alertType: AlertsType.Error,
        message: `Error: ${err}`,
      }

      showAlert(alertPayload)
    }
    onCloseDialog()
  }

  async function onSelectRow(id) {
    try {
      setOpenDialog(true)
      setIsDialogLoading(true)
      const result = {}
      setSchedule(result)
      setDialogTitle('Solicitud guardada')
    } catch (err) {
      const alertPayload = {
        alertType: AlertsType.Error,
        message: `Error: ${err}`,
      }

      showAlert(alertPayload)
    }
    setIsDialogLoading(false)
  }

  return (
    <>
      <EntityLayout
        header={
          <Header
            entity={entity}
            placeholder="Buscar por nombre o rut"
            onSearch={onSearch}
          />
        }
        results={
          <ResultsContainer
            isAllowed={viewList}
            pagination={
              <PaginationContainer
                location="bottom"
                rowsName={entity}
                rowsPerPageOptions={[10, 25, 50, 100]}
              />
            }
            searchBy="description"
            searchCriteria={searchCriteria}
            onSelectRow={onSelectRow}
          />
        }
        toolbar={
          <Toolbar
            isAddButtonActionAllowed={addSchedule}
            pagination={
              <PaginationContainer location="top" rowsName={entity} />
            }
            onClick={onHandleOpenDialog}
          />
        }
        userSettingsContext={userSettingsContext}
      />
      {viewDetail && addSchedule && (
        <ScheduleForm
          isLoading={isDialogLoading}
          isSwitchAllowed={activateSwitch}
          open={openDialog}
          schedule={schedule}
          title={dialogTitle}
          onClose={onCloseDialog}
          onSchedule={onSchedule}
          onUpdate={onUpdate}
        />
      )}
    </>
  )
}

Schedules.defaultProps = {
  searchCriteria: { value: '' },
  userRole: null,
}

Schedules.propTypes = {
  entity: PropTypes.string.isRequired,
  loadConfiguration: PropTypes.func.isRequired,
  searchCriteria: PropTypes.shape({
    value: PropTypes.string.isRequired,
  }),
  showAlert: PropTypes.func.isRequired,
  userRole: PropTypes.instanceOf(Object),
  onSearch: PropTypes.func.isRequired,
}

export default Schedules
