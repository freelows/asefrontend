import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { results } from 'store/modules'
import { EntityPagination } from 'components/layout/Entity'

const { getTotal, getPage, getRowsPerPage } = results.selectors
const { updatePage, updateRowsPerPage } = results.actions

const mapStateToProps = (state) => ({
  page: getPage(state),
  rowsPerPage: getRowsPerPage(state),
  rowsTotal: getTotal(state),
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      onChangePage: updatePage,
      onChangeRowsPerPage: updateRowsPerPage,
    },
    dispatch
  )

const PaginationContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(EntityPagination)

export default PaginationContainer
