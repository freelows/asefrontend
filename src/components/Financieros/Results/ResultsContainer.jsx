import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { results, filters } from 'store/modules'
import Results from './Results'

const {
  getFetching,
  getResults,
  getSort,
  getColumns,
  getPagination,
} = results.selectors
const { updateSort } = results.actions

const { getAll } = filters.selectors

const mapStateToProps = (state) => ({
  loading: getFetching(state),
  results: getResults(state),
  sort: getSort(state),
  columns: [
    {
      name: 'id',
      label: 'Id',
      isVisible: true,
      isSortable: false,
      isAsc: false,
      align: 'left',
      defaultSort: false,
      cell: { type: 'singleText', truncateText: true, maxWidth: 100 },
    },
    {
      label: 'Rut',
      name: 'rut',
      isVisible: true,
      isSortable: false,
      isAsc: false,
      align: 'left',
      defaultSort: false,
      cell: { type: 'singleText', truncateText: true },
    },
    {
      label: 'Cliente',
      name: 'cliente',
      isVisible: true,
      isSortable: false,
      isAsc: false,
      align: 'left',
      defaultSort: false,
      cell: { type: 'singleText', truncateText: true, maxWidth: 400 },
    },
    {
      name: 'montoAuto',
      label: 'Monto Auto',
      isVisible: true,
      isSortable: false,
      isAsc: false,
      align: 'left',
      defaultSort: false,
      cell: { type: 'singleText', truncateText: true, maxWidth: 400 },
    },
    {
      name: 'pie',
      label: 'Pie',
      isVisible: true,
      isSortable: false,
      isAsc: false,
      align: 'left',
      defaultSort: false,
      cell: { type: 'singleText', truncateText: true, maxWidth: 400 },
    },
    {
      name: 'ingresoLiquido',
      label: 'Ingreso Líquido',
      isVisible: true,
      isSortable: false,
      isAsc: false,
      align: 'left',
      defaultSort: false,
      cell: { type: 'singleText', truncateText: true, maxWidth: 400 },
    },
    {
      name: 'plazo',
      label: 'Plazo',
      isVisible: true,
      isSortable: false,
      isAsc: false,
      align: 'left',
      defaultSort: false,
      cell: { type: 'singleText', truncateText: true, maxWidth: 400 },
    },
    {
      name: 'bancoFinanciamientio',
      label: 'Banco Financiamiento',
      isVisible: true,
      isSortable: false,
      isAsc: false,
      align: 'left',
      defaultSort: false,
      cell: { type: 'singleText', truncateText: true, maxWidth: 400 },
    },
  ],
  paging: getPagination(state),
  filters: getAll(state),
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ onSort: updateSort }, dispatch)

const ResultsContainer = connect(mapStateToProps, mapDispatchToProps)(Results)

export default ResultsContainer
