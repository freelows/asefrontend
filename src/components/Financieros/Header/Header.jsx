import React from 'react'
import PropTypes from 'prop-types'

import { EntityHeader } from 'components/layout/Entity'

const Header = ({ entity, ...props }) => {
  return <EntityHeader title="Asesoramientos Financieros" {...props} />
}

Header.propTypes = {
  entity: PropTypes.string.isRequired,
}

export default Header
