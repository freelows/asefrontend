import styled from 'styled-components'
import { Grid } from '@material-ui/core'

export const Header = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  box-shadow: ${(props) => props.theme.shadows[4]};
`

export const HeadGrid = styled(Grid)`
  padding: ${(props) => props.theme.spacing(2.5, 2.5, 1.5, 2.5)};
`
export const NoOverflowGrid = styled(Grid)`
  overflow: hidden;
`
