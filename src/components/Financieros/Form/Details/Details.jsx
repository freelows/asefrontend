import React, { useState } from 'react'

import { Search as SearchIcon } from '@material-ui/icons'

import {
  FormControl,
  IconButton,
  Grid,
  TextField,
  RadioGroup,
  Radio,
  FormLabel,
} from '@material-ui/core'

import { AsesoriaFinancieraFormField } from 'models'
import { MenuItem } from 'components/ui'

import PropTypes from 'prop-types'
import FormInput from '../FormInput'
import FormControlLabel from './styles'

const Details = ({ errors, handleChange, isReadonly, initialState }) => {
  const {
    nombres,
    apellidos,
    rut,
    montoAuto,
    pie,
    ingresoLiquido,
    plazo,
    bancoFinanciamientio,
  } = initialState

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormLabel>Cliente</FormLabel>
        </Grid>
        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={nombres}
            disabled={isReadonly}
            errors={errors}
            fieldName={AsesoriaFinancieraFormField.Nombres}
            handleChange={handleChange}
            label="Nombre"
            placeholder="Ingrese su nombre porfavor"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={apellidos}
            disabled={isReadonly}
            errors={errors}
            fieldName={AsesoriaFinancieraFormField.Apellidos}
            handleChange={handleChange}
            label="Apellido"
            placeholder="Ingrese su apellido porfavor"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={rut}
            disabled={isReadonly}
            errors={errors}
            fieldName={AsesoriaFinancieraFormField.Rut}
            handleChange={handleChange}
            label="Rut"
            placeholder="Ingrese su rut porfavor"
          />
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormLabel></FormLabel>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormLabel>Datos Adicionales</FormLabel>
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={montoAuto}
            disabled={isReadonly}
            errors={errors}
            fieldName={AsesoriaFinancieraFormField.MontoAuto}
            handleChange={handleChange}
            label="Monto"
            placeholder="Ex: 12.000.000"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={pie}
            disabled={isReadonly}
            errors={errors}
            fieldName={AsesoriaFinancieraFormField.Pie}
            handleChange={handleChange}
            label="Pie"
            placeholder="Ex: 3.000.000"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={ingresoLiquido}
            disabled={isReadonly}
            errors={errors}
            fieldName={AsesoriaFinancieraFormField.IngresoLiquido}
            handleChange={handleChange}
            label="Ingreso"
            placeholder="Ex: 1.000.000"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={plazo}
            disabled={isReadonly}
            errors={errors}
            fieldName={AsesoriaFinancieraFormField.Plazo}
            handleChange={handleChange}
            label="Plazo"
            placeholder="Ex: 24 meses"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={bancoFinanciamientio}
            disabled={isReadonly}
            errors={errors}
            fieldName={AsesoriaFinancieraFormField.BancoFinanciamientio}
            handleChange={handleChange}
            label="Financiera"
            placeholder="Ex: Banco Bci"
          />
        </Grid>
      </Grid>
    </>
  )
}

Details.defaultProps = {
  isReadonly: false,
  handleChange: () => {},
  initialState: {
    nombres: '',
    apellidos: '',
    rut: '',
    montoAuto: '',
    pie: '',
    ingresoLiquido: '',
    plazo: '',
    bancoFinanciamientio: '',
  },
}

Details.propTypes = {
  isReadonly: PropTypes.bool,
  errors: PropTypes.instanceOf(Object).isRequired,
  handleChange: PropTypes.func,
  initialState: PropTypes.shape({
    nombres: PropTypes.string,
    apellidos: PropTypes.string,
    rut: PropTypes.string,
    montoAuto: PropTypes.string,
    pie: PropTypes.string,
    ingresoLiquido: PropTypes.string,
    plazo: PropTypes.string,
    bancoFinanciamientio: PropTypes.string,
  }),
}

export default React.memo(Details)
