import React from 'react'

import { Grid } from '@material-ui/core'

import PropTypes from 'prop-types'

const Add = ({ details, handleSubmit }) => {
  return (
    <form noValidate id="schedule-form" onSubmit={handleSubmit}>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          {details}
        </Grid>
      </Grid>
    </form>
  )
}

Add.propTypes = {
  details: PropTypes.node.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}
export default Add
