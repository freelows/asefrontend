import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

import { useForm } from 'hooks'
import {
  AsesoriaFinancieraFormField,
  FinancierasType,
  InspeccionFormField,
} from 'models'

import {
  Dialog,
  Paper,
  DialogTitle,
  DialogContent,
  DialogActions,
  Grid,
  FormControlLabel,
} from '@material-ui/core'

import { CloseRounded as CloseIcon } from '@material-ui/icons'
import { DialogFloatingButton } from 'components/ui'
import validateAsesoramientoFinanciero from 'utils/validateAsesoramientoFinanciero'

import Add from './Add'
import Details from './Details'
import Loader from './Loader'

import { Navigation, StyledPaper, StyledButton, StyledSwitch } from './styles'

const Form = ({
  title,
  open,
  onClose,
  schedule,
  onSchedule,
  isLoading,
  onUpdate,
  isSwitchAllowed,
}) => {
  const stateMock = {
    nombres: '',
    apellidos: '',
    rut: '',
    marca: '',
    modelo: '',
    patente: '',
    anio: '',
    ubicacionVehiculo: '',
  }

  const [initialState, setInitialState] = useState(stateMock)

  useEffect(() => {
    setInitialState(schedule)
  }, [schedule])

  const { errors, hasErrors, handleChange, handleSubmit, resetValue } = useForm(
    {
      initialState,
      onSubmit: (values) => onSchedule(values),
      additionalValidations: validateAsesoramientoFinanciero,
    }
  )

  const resetFields = (...args) => {
    args.forEach((fieldName) => {
      resetValue({ fieldName, shouldValidate: false })
    })
  }

  const resetAndValidateFields = (...args) => {
    args.forEach((fieldName) => {
      resetValue({ fieldName, shouldValidate: true })
    })
  }

  useEffect(() => {
    if (open && !isLoading) {
      setInitialState(stateMock)
      resetAndValidateFields(InspeccionFormField.Nombres)
      resetFields(InspeccionFormField.Nombres)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open])

  const handleOnUpdate = () => {
    onUpdate(schedule)
  }

  return (
    <Dialog
      disableBackdropClick
      aria-labelledby="form-dialog-title"
      maxWidth="md"
      open={open}
      PaperComponent={StyledPaper}
      scroll="paper"
      onClose={onClose}
    >
      {isLoading ? (
        <Loader />
      ) : (
        open && (
          <>
            <Paper elevation={24}>
              <DialogTitle>
                <Grid container>
                  <Grid item xs={11}>
                    {title}
                  </Grid>
                  <Grid item xs={1}></Grid>
                </Grid>
              </DialogTitle>
              <DialogContent dividers>
                <Add
                  details={
                    <Details errors={errors} handleChange={handleChange} />
                  }
                  handleSubmit={handleSubmit}
                />
              </DialogContent>
              <DialogActions>
                <StyledButton onClick={onClose}>Cancelar</StyledButton>
                <StyledButton
                  color="primary"
                  disabled={hasErrors || !!schedule}
                  form="schedule-form"
                  type="submit"
                >
                  Guardar
                </StyledButton>
              </DialogActions>
            </Paper>
            <Navigation>
              <DialogFloatingButton title="Close or use ESC" onClick={onClose}>
                <CloseIcon />
              </DialogFloatingButton>
            </Navigation>
          </>
        )
      )}
    </Dialog>
  )
}

Form.defaultProps = {
  schedule: null,
  isLoading: false,
  onUpdate: () => {},
  title: '',
  isSwitchAllowed: true,
}

Form.propTypes = {
  isLoading: PropTypes.bool,
  isSwitchAllowed: PropTypes.bool,
  open: PropTypes.bool.isRequired,
  schedule: PropTypes.shape({
    nombre: PropTypes.string,
  }),
  title: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  onSchedule: PropTypes.func.isRequired,
  onUpdate: PropTypes.func,
}

export default Form
