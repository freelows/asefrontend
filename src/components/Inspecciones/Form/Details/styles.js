import styled from 'styled-components'

import { FormControlLabel } from '@material-ui/core'

export default styled(FormControlLabel)`
  margin-right: 6px;
`
