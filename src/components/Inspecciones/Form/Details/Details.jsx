import React, { useState } from 'react'

import { Search as SearchIcon } from '@material-ui/icons'

import {
  FormControl,
  IconButton,
  Grid,
  TextField,
  RadioGroup,
  Radio,
  FormLabel,
} from '@material-ui/core'

import { InspeccionFormField } from 'models'
import { MenuItem } from 'components/ui'

import PropTypes from 'prop-types'
import FormInput from '../FormInput'
import FormControlLabel from './styles'

const Details = ({ errors, handleChange, isReadonly, initialState }) => {
  const {
    nombres,
    apellidos,
    rut,
    marca,
    modelo,
    patente,
    anio,
    ubicacionVehiculo,
  } = initialState

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormLabel>Cliente</FormLabel>
        </Grid>
        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={nombres}
            disabled={isReadonly}
            errors={errors}
            fieldName={InspeccionFormField.Nombres}
            handleChange={handleChange}
            label="Nombre"
            placeholder="Ingrese su nombre porfavor"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={apellidos}
            disabled={isReadonly}
            errors={errors}
            fieldName={InspeccionFormField.Apellidos}
            handleChange={handleChange}
            label="Apellido"
            placeholder="Ingrese su apellido porfavor"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={rut}
            disabled={isReadonly}
            errors={errors}
            fieldName={InspeccionFormField.Rut}
            handleChange={handleChange}
            label="Rut"
            placeholder="Ingrese su rut porfavor"
          />
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormLabel></FormLabel>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormLabel>Datos Adicionales</FormLabel>
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={marca}
            disabled={isReadonly}
            errors={errors}
            fieldName={InspeccionFormField.Marca}
            handleChange={handleChange}
            label="Marca"
            placeholder="Marca"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={modelo}
            disabled={isReadonly}
            errors={errors}
            fieldName={InspeccionFormField.Modelo}
            handleChange={handleChange}
            label="Modelo"
            placeholder="Modelo"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={patente}
            disabled={isReadonly}
            errors={errors}
            fieldName={InspeccionFormField.Patente}
            handleChange={handleChange}
            label="Patente"
            placeholder="Patente"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={anio}
            disabled={isReadonly}
            errors={errors}
            fieldName={InspeccionFormField.Anio}
            handleChange={handleChange}
            label="Año"
            placeholder="Año"
          />
        </Grid>

        <Grid item xs={4}>
          <FormInput
            validate
            defaultValue={ubicacionVehiculo}
            disabled={isReadonly}
            errors={errors}
            fieldName={InspeccionFormField.UbicacionVehiculo}
            handleChange={handleChange}
            label="Financiera"
            placeholder="Ubicación"
          />
        </Grid>
      </Grid>
    </>
  )
}

Details.defaultProps = {
  isReadonly: false,
  handleChange: () => {},
  initialState: {
    nombres: '',
    apellidos: '',
    rut: '',
    marca: '',
    modelo: '',
    patente: '',
    anio: '',
    ubicacionVehiculo: '',
  },
}

Details.propTypes = {
  isReadonly: PropTypes.bool,
  errors: PropTypes.instanceOf(Object).isRequired,
  handleChange: PropTypes.func,
  initialState: PropTypes.shape({
    nombres: PropTypes.string,
    apellidos: PropTypes.string,
    rut: PropTypes.string,
    marca: PropTypes.string,
    modelo: PropTypes.string,
    patente: PropTypes.string,
    anio: PropTypes.string,
    ubicacionVehiculo: PropTypes.string,
  }),
}

export default React.memo(Details)
