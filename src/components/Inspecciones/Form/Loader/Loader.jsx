import React from 'react'

import { Box } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { Header, HeadGrid, NoOverflowGrid } from './styles'

const Loader = () => (
  <>
    <Header>
      <HeadGrid container alignItems="center" wrap="nowrap">
        <NoOverflowGrid container item wrap="nowrap">
          <Box marginRight={2}>
            <Skeleton height={50} width={50} />
          </Box>
        </NoOverflowGrid>
      </HeadGrid>
    </Header>
  </>
)

export default Loader
