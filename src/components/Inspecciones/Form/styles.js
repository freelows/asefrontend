import styled from 'styled-components'
import { Paper, Button, Switch } from '@material-ui/core'

export const Navigation = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 8px;
  color: #fff;
`

export const StyledPaper = styled(Paper)`
  background-color: transparent;
  box-shadow: none;
  flex-direction: row;
  margin: 0;
  overflow: hidden;
  > .MuiPaper-root {
    flex: 1;
    overflow-x: hidden;
    overflow-y: auto;
  }
`
export const StyledButton = styled(Button)`
  padding: 6px 16px;
  border-radius: 4px;
`
export const StyledSwitch = styled(Switch)`
  &.MuiSwitch-root {
    width: 80px;
    height: 47px;
    padding: 10px;
  }
  .MuiSwitch-track {
    border-radius: 14px;
  }

  .MuiSwitch-thumb {
    width: 21px;
    height: 21px;
  }
  .MuiSwitch-switchBase.Mui-checked {
    left: 100%;
    transform: translateX(-100%);
  }

  .MuiSwitch-switchBase {
    position: absolute;
    padding: 13px;
  }
`
