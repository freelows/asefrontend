import React from 'react'

import { FormControl, TextField, FormHelperText } from '@material-ui/core'

import PropTypes from 'prop-types'

const FormInput = ({
  fieldName,
  errors,
  label,
  handleChange,
  placeholder,
  defaultValue,
  disabled,
  validate,
  value,
}) => {
  return validate ? (
    <FormControl fullWidth required error={!!errors[fieldName]}>
      <TextField
        autoComplete="off"
        defaultValue={defaultValue}
        disabled={disabled}
        error={!!errors[fieldName]}
        inputProps={{ name: fieldName }}
        label={label}
        placeholder={placeholder}
        value={value}
        variant="outlined"
        onChange={handleChange}
      />
      {errors[fieldName] && (
        <FormHelperText>{errors[fieldName]}</FormHelperText>
      )}
    </FormControl>
  ) : (
    <FormControl fullWidth>
      <TextField
        autoComplete="off"
        defaultValue={defaultValue}
        disabled={disabled}
        inputProps={{ name: fieldName }}
        label={label}
        placeholder={placeholder}
        value={value}
        variant="outlined"
        onChange={handleChange}
      />
    </FormControl>
  )
}

FormInput.defaultProps = {
  placeholder: '',
  defaultValue: '',
  errors: null,
  disabled: false,
  label: '',
  validate: false,
  value: undefined,
}

FormInput.propTypes = {
  fieldName: PropTypes.string.isRequired,
  errors: PropTypes.instanceOf(Object),
  label: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  validate: PropTypes.bool,
  value: PropTypes.string,
}

export default React.memo(FormInput)
