import React from 'react'
import PropTypes from 'prop-types'
import fixResults from 'utils/fixResults'
import filterByPropertyString from 'utils/filterByPropertyString'

import { Filter } from 'models'
import { EntityResults } from 'components/layout/Entity'

const Results = ({
  paging,
  searchCriteria,
  searchBy,
  results,
  sort,
  isAllowed,
  ...props
}) => {
  const value = searchCriteria ? searchCriteria.value : ''

  const filteredResults = filterByPropertyString({
    searchBy,
    results,
    searchCriteria: value,
  })

  const fixedResults = fixResults({
    sort,
    paging,
    results: filteredResults,
  })

  return (
    isAllowed && <EntityResults results={fixedResults} sort={sort} {...props} />
  )
}

Results.defaultProps = {
  filters: [],
  searchCriteria: { value: '' },
}

Results.propTypes = {
  results: PropTypes.arrayOf(Object).isRequired,
  paging: PropTypes.instanceOf(Object).isRequired,
  sort: PropTypes.instanceOf(Object).isRequired,
  filters: PropTypes.arrayOf(Filter),
  searchCriteria: PropTypes.shape({
    value: PropTypes.string.isRequired,
  }),
  searchBy: PropTypes.string.isRequired,
}

export default Results
