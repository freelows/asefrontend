// eslint-disable-next-line import/no-extraneous-dependencies
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { filters, ui } from 'store/modules'
import Inspecciones from './Inspecciones'

const { getEntity, getUserRole } = ui.selectors
const { showAlert } = ui.actions
const { fetchConfiguration, updateAdvanced } = filters.actions
const { getAdvancedFilter } = filters.selectors

const mapStateToProps = (state) => ({
  entity: getEntity(state),
  searchCriteria: getAdvancedFilter(state),
  userRole: getUserRole(state),
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      onSearch: updateAdvanced,
      loadConfiguration: fetchConfiguration,
      showAlert,
    },
    dispatch
  )

const InspeccionesContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Inspecciones)

export default InspeccionesContainer
