import styled from 'styled-components'
import { Button } from '@material-ui/core'

export default styled(Button)`
  padding: 6px 16px;
  border-radius: 4px;
  background-color: ${(props) => props.theme.palette.primary.main};

  &:hover {
    background-color: ${(props) => props.theme.palette.primary.dark};
  }
`
