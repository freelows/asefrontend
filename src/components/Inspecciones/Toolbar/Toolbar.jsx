import React from 'react'
import { PrimaryButton } from 'components/ui'
import { EntityToolbar } from 'components/layout/Entity'

import PropTypes from 'prop-types'

const Toolbar = (props) => {
  const { onClick, isAddButtonActionAllowed } = props

  const button = isAddButtonActionAllowed && (
    <PrimaryButton label="+ Crear" variant="contained" onClick={onClick} />
  )

  return <EntityToolbar options={[]} specialAction={button} {...props} />
}

Toolbar.defaultProps = {
  isAddButtonActionAllowed: true,
}

Toolbar.propTypes = {
  isAddButtonActionAllowed: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
}

export default Toolbar
