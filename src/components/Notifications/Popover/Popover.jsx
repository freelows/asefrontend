import React from 'react'
import PropTypes from 'prop-types'

import { StyledPopover, StyledTypography } from './styles'

const Popover = ({ children, ...muiPopoverProps }) => {
  const { anchorEl, onClose } = muiPopoverProps

  return (
    <StyledPopover
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      getContentAnchorEl={null}
      id="notifications-popover"
      open={!!muiPopoverProps.anchorEl}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      onClose={onClose}
    >
      <StyledTypography variant="h6">Notifications</StyledTypography>
      {children}
    </StyledPopover>
  )
}

Popover.propTypes = {
  children: PropTypes.node.isRequired,
}

export default React.memo(Popover)
