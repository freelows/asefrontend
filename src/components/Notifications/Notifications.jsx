import React, {
  useState,
  Suspense,
  useCallback,
  useEffect,
  useContext,
} from 'react'
import PropTypes from 'prop-types'

import AuthenticationContext from 'context/auth'
import { NotificationsType } from 'models'
import NotificationsPopover from './Popover'
import NotificationButton from './Button'

const NotificationsContent = React.lazy(() => import('./Content/Content'))

const Notifications = ({
  allNotifications,
  userNotifications,
  newUserNotificationsTotal,
  newAllNotificationsTotal,
  loadNotifications,
  markAsRead,
  disable,
}) => {
  const [anchorEl, setAnchorEl] = useState(null)
  const {
    _user: { userName },
  } = useContext(AuthenticationContext)

  const handleClosePopover = () => setAnchorEl(null)
  const handleOpenPopover = useCallback(
    (event) => setAnchorEl(event.currentTarget),
    []
  )

  const disableUserNotification = (id) => disable(id, NotificationsType.User)
  const markAsReadUserNotification = (id) =>
    markAsRead(id, NotificationsType.User)

  useEffect(() => {
    loadNotifications(userName)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const newCounter = newAllNotificationsTotal + newUserNotificationsTotal

  return (
    <>
      <NotificationButton
        handleOpenPopover={handleOpenPopover}
        newCounter={newCounter}
      />
      <NotificationsPopover anchorEl={anchorEl} onClose={handleClosePopover}>
        <Suspense fallback={<div />}>
          <NotificationsContent
            allNotificationsProps={{
              items: allNotifications,
              unreadTotal: newAllNotificationsTotal,
            }}
            userNotificationsProps={{
              items: userNotifications,
              unreadTotal: newUserNotificationsTotal,
              onRemove: disableUserNotification,
              onRead: markAsReadUserNotification,
            }}
          />
        </Suspense>
      </NotificationsPopover>
    </>
  )
}

Notifications.defaultProps = {
  allNotifications: [],
  userNotifications: [],
  newUserNotificationsTotal: 0,
  newAllNotificationsTotal: 0,
}

Notifications.propTypes = {
  allNotifications: PropTypes.arrayOf(Object),
  userNotifications: PropTypes.arrayOf(Object),
  newUserNotificationsTotal: PropTypes.number,
  newAllNotificationsTotal: PropTypes.number,
  loadNotifications: PropTypes.func.isRequired,
  markAsRead: PropTypes.func.isRequired,
  disable: PropTypes.func.isRequired,
}

export default React.memo(Notifications)
