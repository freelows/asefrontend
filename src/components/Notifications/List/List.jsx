import React from 'react'
import PropTypes from 'prop-types'
import { formatDistanceToNow } from 'date-fns'

import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  ListItemAvatar,
  Divider,
} from '@material-ui/core'
import {
  CloudDownload as CloudDownloadIcon,
  FiberManualRecord as FiberManualRecordIcon,
} from '@material-ui/icons'
import ChatIcon from '@material-ui/icons/ChatTwoTone'

import { EmptySection, Tooltip } from 'components/ui'
import { StyledAvatar, StyledList } from './styles'

export const NoItems = () => (
  <EmptySection
    stretch
    description="You don't have any notifications yet"
    icon={ChatIcon}
  />
)

const List = ({ items, onRead }) => {
  return items.length ? (
    <StyledList>
      {items.map(({ id, dateTime, message, read, hyperlink }, index) => {
        const downloadFile = () => window.open(hyperlink, '_blank')

        const handleOnMouseOver = () => onRead && !read && onRead(id)

        return (
          <React.Fragment key={id}>
            <ListItem
              onFocus={handleOnMouseOver}
              onMouseOver={handleOnMouseOver}
            >
              <ListItemAvatar>
                <StyledAvatar>
                  <FiberManualRecordIcon
                    color={read ? 'disabled' : 'secondary'}
                    fontSize="small"
                  />
                </StyledAvatar>
              </ListItemAvatar>
              <ListItemText
                primary={message}
                secondary={formatDistanceToNow(new Date(dateTime), {
                  addSuffix: true,
                  includeSeconds: true,
                })}
              />
              {hyperlink && (
                <ListItemSecondaryAction>
                  <Tooltip title="Download">
                    <IconButton aria-label="download" onClick={downloadFile}>
                      <CloudDownloadIcon />
                    </IconButton>
                  </Tooltip>
                </ListItemSecondaryAction>
              )}
            </ListItem>
            {index < items.length - 1 && <Divider component="li" />}
          </React.Fragment>
        )
      })}
    </StyledList>
  ) : (
    <NoItems />
  )
}

List.defaultProps = {
  onRead: null,
}

List.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      message: PropTypes.string,
      dateTime: PropTypes.string,
      read: PropTypes.bool,
      hyperlink: PropTypes.string,
    })
  ).isRequired,
  onRead: PropTypes.func,
}

export default React.memo(List)
