import styled from 'styled-components'
import { Avatar, List } from '@material-ui/core'

export const StyledAvatar = styled(Avatar)`
  background-color: transparent;
`

export const StyledList = styled(List)`
  max-height: 350px;
  overflow-y: auto;

  > li:hover {
    transition: background-color 250ms ease-in-out;
    background-color: ${(props) => props.theme.palette.action.hover};
  }
`
