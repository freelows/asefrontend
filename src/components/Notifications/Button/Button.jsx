import React from 'react'
import PropTypes from 'prop-types'

import { Tooltip } from 'components/ui'

import { IconButton, Badge } from '@material-ui/core'
import { Notifications as NotificationsIcon } from '@material-ui/icons'

const Button = ({ handleOpenPopover, newCounter }) => {
  return (
    <Tooltip title="Notifications">
      <IconButton
        aria-label="View Notifications"
        color="inherit"
        onClick={handleOpenPopover}
      >
        <Badge
          badgeContent={newCounter}
          color="secondary"
          invisible={!newCounter}
        >
          <NotificationsIcon />
        </Badge>
      </IconButton>
    </Tooltip>
  )
}

Button.propTypes = {
  handleOpenPopover: PropTypes.func.isRequired,
  newCounter: PropTypes.number.isRequired,
}

export default React.memo(Button)
