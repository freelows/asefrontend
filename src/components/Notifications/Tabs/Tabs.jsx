import React, { useState, Fragment } from 'react'
import PropTypes from 'prop-types'
import SwipeableViews from 'react-swipeable-views'

import { Tabs as MuiTabs, Tab } from '@material-ui/core'
import { TabPanel } from 'components/ui'

const Tabs = ({ userPanel, allPanel, userLabel, allLabel }) => {
  const [value, setValue] = useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleChangeIndex = (index) => {
    setValue(index)
  }

  return (
    <>
      <MuiTabs
        aria-label="notification tabs"
        indicatorColor="primary"
        textColor="primary"
        value={value}
        variant="fullWidth"
        onChange={handleChange}
      >
        <Tab label={userLabel} value={0} />
        <Tab label={allLabel} value={1} />
      </MuiTabs>
      <SwipeableViews axis="x" index={value} onChangeIndex={handleChangeIndex}>
        <TabPanel index={0} value={value}>
          {userPanel}
        </TabPanel>
        <TabPanel index={1} value={value}>
          {allPanel}
        </TabPanel>
      </SwipeableViews>
    </>
  )
}

Tabs.propTypes = {
  userPanel: PropTypes.node.isRequired,
  allPanel: PropTypes.node.isRequired,
  userLabel: PropTypes.string.isRequired,
  allLabel: PropTypes.string.isRequired,
}

export default React.memo(Tabs)
