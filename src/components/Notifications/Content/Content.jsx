import React from 'react'
import PropTypes from 'prop-types'

import NotificationsTabs from '../Tabs'
import NotificationsList from '../List'

function parseCounter(value) {
  return value > 0 ? `(${value})` : ''
}

const Content = ({ userNotificationsProps, allNotificationsProps }) => {
  const userCounter = parseCounter(userNotificationsProps.unreadTotal)
  const allCounter = parseCounter(allNotificationsProps.unreadTotal)

  return (
    <NotificationsTabs
      allLabel={`All ${allCounter}`}
      allPanel={<NotificationsList {...allNotificationsProps} />}
      userLabel={`Mine ${userCounter}`}
      userPanel={<NotificationsList {...userNotificationsProps} />}
    />
  )
}

Content.propTypes = {
  userNotificationsProps: PropTypes.shape({
    items: PropTypes.arrayOf(PropTypes.object),
    unreadTotal: PropTypes.number,
    onRemove: PropTypes.func,
    onRead: PropTypes.func,
  }).isRequired,
  allNotificationsProps: PropTypes.shape({
    items: PropTypes.arrayOf(PropTypes.object),
    unreadTotal: PropTypes.number,
    onRemove: PropTypes.func,
    onRead: PropTypes.func,
  }).isRequired,
}

export default React.memo(Content)
