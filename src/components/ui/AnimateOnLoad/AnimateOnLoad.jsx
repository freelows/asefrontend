import React from 'react'
import PropTypes from 'prop-types'

import { useTransition, animated } from 'react-spring'

const fadeAnimation = {
  from: { opacity: 0 },
  enter: { position: 'initial', opacity: 1 },
  leave: { position: 'absolute', opacity: 0 },
}

const AnimateOnLoad = ({
  loading,
  hasResults,
  loader,
  noResults,
  children,
}) => {
  const transitions = useTransition(loading, null, fadeAnimation)

  return transitions.map(({ item, key, props }) =>
    item ? (
      <animated.div key={key} style={props}>
        {loader}
      </animated.div>
    ) : (
      <animated.div key={key} style={props}>
        {!hasResults ? noResults : children}
      </animated.div>
    )
  )
}

AnimateOnLoad.propTypes = {
  loading: PropTypes.bool.isRequired,
  hasResults: PropTypes.bool.isRequired,
  loader: PropTypes.element.isRequired,
  noResults: PropTypes.element.isRequired,
  children: PropTypes.element.isRequired,
}

export default React.memo(AnimateOnLoad)
