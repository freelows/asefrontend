import React from 'react'
import PropTypes from 'prop-types'

import { Typography } from '@material-ui/core'

import { Root, IconWrapper, StyledTypography, Action } from './styles'

const EmptySection = ({
  action,
  icon: Icon,
  title,
  description,
  background,
  stretch
}) => {
  return (
    <Root background={background} stretch={stretch}>
      <IconWrapper>
        <Icon color="disabled" />
      </IconWrapper>
      <div>
        {title && (
          <StyledTypography color="textPrimary" component="h3" variant="h5">
            {title}
          </StyledTypography>
        )}
        {description && (
          <Typography color="textSecondary" variant="body2">
            {description}
          </Typography>
        )}
      </div>
      {action && <Action>{action}</Action>}
    </Root>
  )
}

EmptySection.defaultProps = {
  action: null,
  background: false,
  stretch: false,
  title: '',
  description: ''
}

EmptySection.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  icon: PropTypes.instanceOf(Object).isRequired,
  action: PropTypes.node,
  background: PropTypes.bool,
  stretch: PropTypes.bool
}

export default React.memo(EmptySection)
