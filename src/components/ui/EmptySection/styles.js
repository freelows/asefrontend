import styled, { css } from 'styled-components'
import { Typography } from '@material-ui/core'

export const Root = styled.div`
  padding-top: ${props => `${props.theme.spacing(3)}px`};
  min-height: 220px;
  display: flex;
  align-items: center;
  flex-flow: column;
  text-align: center;
  border-radius: 4px 4px 0 0;
  ${props =>
    props.background &&
    css`
      background-image: linear-gradient(
        -180deg,
        ${props => props.theme.palette.action.hover} 0,
        ${props => props.theme.palette.background.default} 100%
      );
    `}

  ${props =>
    props.stretch &&
    css`
      padding-top: 0;
      height: 100%;
      justify-content: center;
    `}
`

export const IconWrapper = styled.div`
  svg {
    min-width: 48px;
    min-height: 48px;
  }
`

export const StyledTypography = styled(Typography)`
  padding-top: ${props => `${props.theme.spacing(1)}px`};
`

export const Action = styled.div`
  margin-top: ${props => `${props.theme.spacing(2.5)}px`};
`
