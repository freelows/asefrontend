import React from 'react'

import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  ListItemAvatar,
  Box,
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'

const Loader = () => (
  <React.Fragment>
    <Box marginX={2}>
      <Skeleton height="10" width="100%" />
    </Box>
    <List>
      {[0, 1, 3].map((value) => (
        <ListItem key={value}>
          <ListItemAvatar>
            <Skeleton height={40} variant="circle" width={40} />
          </ListItemAvatar>
          <ListItemText
            disableTypography
            primary={<Skeleton height={8} width="40%" />}
            secondary={<Skeleton height={8} width="90%" />}
          />
          <ListItemSecondaryAction>
            <Box marginRight={2}>
              <Skeleton height={15} variant="rect" width={15} />
            </Box>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  </React.Fragment>
)

export default Loader
