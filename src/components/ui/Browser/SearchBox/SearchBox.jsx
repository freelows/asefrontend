import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'

import debounce from 'lodash.debounce'

import { InputBase, IconButton } from '@material-ui/core'
import { Search as SearchIcon, Clear as ClearIcon } from '@material-ui/icons'
import { PrefixIconWrapper, Wrapper, Root } from './styles'

const SearchBox = ({ value, placeholder, onChange }) => {
  let textFieldInputRef = useRef(null)
  const hasValue = !!value
  const [hasFocus, setHasFocus] = useState(false)
  const debouncedOnChange = debounce(onChange, 300)

  const handleOnChange = (e) => {
    e.persist()
    return debouncedOnChange(e.target.value)
  }

  const handleToggleFocus = () => !hasValue && setHasFocus(!hasFocus)

  const clear = () => {
    textFieldInputRef.current.value = ''
    onChange('')
    textFieldInputRef.current.focus()
  }

  return (
    <Root hasFocus={hasFocus || hasValue}>
      <Wrapper>
        <PrefixIconWrapper>
          <SearchIcon color={hasFocus || hasValue ? 'primary' : 'disabled'} />
        </PrefixIconWrapper>
        <InputBase
          fullWidth
          inputProps={{ 'aria-label': 'search' }}
          inputRef={textFieldInputRef}
          placeholder={placeholder}
          onBlur={handleToggleFocus}
          onChange={handleOnChange}
          onFocus={handleToggleFocus}
        />
        {hasValue && (
          <IconButton arial-label="Clear" size="small" onClick={clear}>
            <ClearIcon color="action" fontSize="inherit" />
          </IconButton>
        )}
      </Wrapper>
    </Root>
  )
}

SearchBox.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
}

export default SearchBox
