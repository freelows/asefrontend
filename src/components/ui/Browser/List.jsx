import React from 'react'
import PropTypes from 'prop-types'

import {
  List as MuiList,
  Checkbox,
  ListItemText,
  ListItemSecondaryAction,
  Divider,
  ListItemAvatar,
} from '@material-ui/core'
import { Star as StarIcon } from '@material-ui/icons'
import { UserName, StyledAvatar, StyledListItem } from './styles'

const List = ({ items, onSelect, isSelected, showSecondaryActions }) => {
  return (
    <MuiList>
      {items.map((item, index) => {
        const labelId = `favorite-list-label-${item.id}`

        return (
          <React.Fragment key={item.id}>
            <StyledListItem
              key={item.id}
              button
              role={undefined}
              onClick={onSelect(item.id)}
            >
              <ListItemAvatar>
                <StyledAvatar className={item.public ? 'public' : undefined}>
                  <StarIcon />
                </StyledAvatar>
              </ListItemAvatar>
              <ListItemText
                id={labelId}
                primary={item.label}
                secondary={
                  <React.Fragment>
                    {item.public && (
                      <React.Fragment>
                        <UserName
                          color="textPrimary"
                          component="span"
                          variant="body2"
                        >
                          {item.owner}
                        </UserName>
                        —
                      </React.Fragment>
                    )}
                    {item.description}
                  </React.Fragment>
                }
              />
              {showSecondaryActions && (
                <ListItemSecondaryAction>
                  <Checkbox
                    disableRipple
                    checked={isSelected(item.id)}
                    color="primary"
                    inputProps={{ 'aria-labelledby': labelId }}
                    tabIndex={-1}
                    onChange={onSelect(item.id)}
                  />
                </ListItemSecondaryAction>
              )}
            </StyledListItem>
            {index < items.length - 1 && (
              <Divider component="li" variant="inset" />
            )}
          </React.Fragment>
        )
      })}
    </MuiList>
  )
}

List.defaultProps = {
  showSecondaryActions: false,
}

List.propTypes = {
  isSelected: PropTypes.func.isRequired,
  items: PropTypes.instanceOf(Array).isRequired,
  showSecondaryActions: PropTypes.bool,
  onSelect: PropTypes.func.isRequired,
}

export default List
