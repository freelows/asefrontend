import React from 'react'
import PropTypes from 'prop-types'

import {
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  Divider,
} from '@material-ui/core'
import {
  StarBorder as StarBorderIcon,
  ErrorOutline as ErrorIcon,
  Refresh as RefreshIcon,
} from '@material-ui/icons'

import { EmptySection, AnimateOnLoad } from 'components/ui'
import List from './List'
import Loader from './Loader'

import {
  StyledDialogContent,
  StyledIconButton,
  StyledCircularProgress,
} from './styles'
import SearchBox from './SearchBox'

const NoResultsFound = () => (
  <EmptySection
    description="Maybe change the search criteria?"
    icon={ErrorIcon}
    title="No favorites found."
  />
)

const NoSourceFound = () => (
  <EmptySection
    description="Start adding filters and save as Favorite!"
    icon={StarBorderIcon}
    title="No saved favorites found."
  />
)

const Browser = ({
  items,
  currentItems,
  open,
  onClose,
  onSelect,
  loading,
  loadFavorites,
}) => {
  const [selectedItems, setSelectedItems] = React.useState(currentItems)
  const [fetching, setFetching] = React.useState(true)
  const [source, setSource] = React.useState([])
  const [filteredItems, setFilteredItems] = React.useState([])
  const [searchTerm, setSearchTerm] = React.useState('')

  const handleCheck = (value) => selectedItems.indexOf(value) !== -1

  const handleOnSelect = (value) => () => {
    onSelect(value)
    onClose()
  }

  const reset = () => {
    setSource([])
    setFetching(true)
    setSelectedItems(currentItems)
  }

  React.useEffect(() => {
    if (open) {
      setFetching(true)

      setTimeout(() => {
        setSource(items)
        setFilteredItems(items)
        setFetching(false)
      }, 3000)
    }
  }, [items, open])

  React.useEffect(() => {
    if (searchTerm && source) {
      const nFilteredItems = source.filter(
        (item) =>
          item.label.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
          item.owner.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
      )
      setFilteredItems(nFilteredItems)
    }
  }, [searchTerm, source])

  return (
    <Dialog
      fullWidth
      aria-labelledby="favorites-browser-title"
      maxWidth="sm"
      open={open}
      onClose={onClose}
      onExited={reset}
    >
      <DialogTitle id="favorites-browser-title">
        Saved Favorites
        {loading ? (
          <StyledCircularProgress />
        ) : (
          <StyledIconButton onClick={loadFavorites}>
            <RefreshIcon />
          </StyledIconButton>
        )}
      </DialogTitle>

      <StyledDialogContent dividers>
        <AnimateOnLoad
          hasResults={!!source.length}
          loader={<Loader />}
          loading={fetching}
          noResults={<NoSourceFound />}
        >
          <React.Fragment>
            <SearchBox
              placeholder="Search by name or author..."
              value={searchTerm}
              onChange={setSearchTerm}
            />
            <Divider />
            {searchTerm && !filteredItems.length ? (
              <NoResultsFound />
            ) : (
              <List
                isSelected={handleCheck}
                items={searchTerm ? filteredItems : source}
                onSelect={handleOnSelect}
              />
            )}
          </React.Fragment>
        </AnimateOnLoad>
      </StyledDialogContent>
      <DialogActions>
        <Button onClick={onClose}>Discard</Button>
      </DialogActions>
    </Dialog>
  )
}

Browser.defaultProps = {
  open: false,
  loading: false,
  currentItems: [],
  loadFavorites: () => {},
}

Browser.propTypes = {
  currentItems: PropTypes.arrayOf(PropTypes.string),
  items: PropTypes.arrayOf(Object).isRequired,
  loadFavorites: PropTypes.func,
  loading: PropTypes.bool,
  open: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
}

export default React.memo(Browser)
