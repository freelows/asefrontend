import styled, { css } from 'styled-components'
import {
  DialogContent,
  Typography,
  Avatar,
  ListItem,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import { ThemeModes } from 'theme/variables'

const RefreshStyles = css`
  position: absolute;
  right: 8px;
  top: 8px;
`

export const StyledDialogContent = styled(DialogContent)`
  padding: 0;
`

export const UserName = styled(Typography)`
  color: ${(props) => props.theme.palette.secondary.light};
`
export const StyledAvatar = styled(Avatar)`
  &:not(.public) {
    background-color: transparent;
    color: ${(props) =>
      props.theme.palette.type === ThemeModes.Light
        ? props.theme.palette.grey['400']
        : props.theme.palette.grey['600']};
  }
`

export const StyledListItem = styled(ListItem)`
  padding-right: 70px;
`

export const StyledIconButton = styled(IconButton)`
  ${RefreshStyles}
`

export const StyledCircularProgress = styled(CircularProgress)`
  ${RefreshStyles}
  color: ${(props) =>
    props.theme.palette.type === ThemeModes.Light ? 'black' : 'white'};
`
