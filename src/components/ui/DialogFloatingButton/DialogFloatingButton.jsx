import React from 'react'
import PropTypes from 'prop-types'

import { Tooltip } from 'components/ui'

import { IconButton } from '@material-ui/core'

const DialogFloatingButton = ({
  title,
  onClick,
  disabled = false,
  children,
}) => {
  return (
    <Tooltip title={title}>
      <span>
        <IconButton color="inherit" disabled={disabled} onClick={onClick}>
          {children}
        </IconButton>
      </span>
    </Tooltip>
  )
}

DialogFloatingButton.defaultProps = {
  disabled: false,
}

DialogFloatingButton.propTypes = {
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

export default DialogFloatingButton
