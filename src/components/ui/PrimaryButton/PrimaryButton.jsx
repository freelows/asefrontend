import React from 'react'

import Button from './styles'

const PrimaryButton = ({ ...props }) => {
  const { label } = props
  return <Button {...props}>{label}</Button>
}

export default PrimaryButton
