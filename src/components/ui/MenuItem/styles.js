import styled from 'styled-components'

import { MenuItem as MuiMenuItem } from '@material-ui/core'

export default styled(MuiMenuItem)`
  display: block;
  padding: 6px;
  padding-left: 16px;
  padding-right: 16px;
`
