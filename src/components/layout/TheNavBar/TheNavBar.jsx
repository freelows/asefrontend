import React, { memo, useContext, Fragment } from 'react'
import PropTypes from 'prop-types'

import {
  AssignmentIndRounded as AssignmentIndIcon,
  WorkRounded as WorkIcon,
  ScheduleRounded as ScheduleIcon,
  LocalAtm,
  EmojiTransportation,
  Inbox as InboxIcon,
} from '@material-ui/icons'

import UserSettingsContext from 'context/userSettings'
import { Tooltip, RippleButton } from 'components/ui'

import { Aside, NavBar, Link, MiniLink, Label, Logo, Brand } from './styles'

const TheNavBar = ({ userRole }) => {
  const { miniNavBar } = useContext(UserSettingsContext)

  let viewLeads
  let viewJobs
  let viewSchedules

  if (userRole) {
    viewLeads = true
    viewJobs = true
    viewSchedules = true
  }

  const pages = [
    {
      title: 'Financiero',
      icon: LocalAtm,
      link: '/Financieros',
      enabled: viewLeads,
    },
    {
      title: 'Inspecciones',
      icon: EmojiTransportation,
      link: '/Inspecciones',
      enabled: viewJobs,
    },
  ]

  return (
    <Aside isMini={miniNavBar}>
      <NavBar>
        <Brand>
          <Logo />
        </Brand>
        {pages.map(
          ({ link, icon: Icon, title, enabled }) =>
            enabled && (
              <Fragment key={title}>
                {miniNavBar ? (
                  <Tooltip key={title} placement="right" title={title}>
                    <div>
                      <RippleButton disabled={!enabled}>
                        <MiniLink to={link}>
                          <Icon />
                        </MiniLink>
                      </RippleButton>
                    </div>
                  </Tooltip>
                ) : (
                  <RippleButton key={title}>
                    <Link to={link}>
                      <Icon />
                      <Label>{title}</Label>
                    </Link>
                  </RippleButton>
                )}
              </Fragment>
            )
        )}
      </NavBar>
    </Aside>
  )
}

TheNavBar.defaultProps = {
  userRole: null,
}

TheNavBar.propTypes = {
  userRole: PropTypes.instanceOf(Object),
}

export default memo(TheNavBar)
