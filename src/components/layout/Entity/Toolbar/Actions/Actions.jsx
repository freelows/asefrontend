import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'

import { ListItemIcon } from '@material-ui/core'
import { Column, ToolbarMenuOption } from 'models'

import MenuButton from './MenuButton'

import ColumnsSorter from './ColumnsSorter'
import ColumnsViewer from './ColumnsViewer'
import Menu from './Menu'
import { Root, StyledMenuItem } from './styles'

const Actions = ({
  columns,
  options,
  onSort,
  onSelect,
  children,
  specialAction,
}) => {
  const [anchorEl, setAnchorEl] = useState(null)

  const handleOnClose = () => setAnchorEl(null)

  const handleOnClick = useCallback((event) => setAnchorEl(event.currentTarget))

  return (
    <Root>
      {children}

      {onSort && <ColumnsSorter items={columns} onSort={onSort} />}

      {onSelect && <ColumnsViewer items={columns} onSelect={onSelect} />}

      {specialAction}

      {options && !!options.length && (
        <React.Fragment>
          <MenuButton handleOnClick={handleOnClick} />
          <Menu anchorEl={anchorEl} handleOnClose={handleOnClose}>
            {options.map(({ id, name, event, icon: Icon, isAllowed }) => {
              const handleClick = () => {
                event()
                handleOnClose()
              }

              return (
                <StyledMenuItem
                  key={`menu-option-${id}`}
                  disabled={!isAllowed}
                  onClick={handleClick}
                >
                  <ListItemIcon>
                    <Icon />
                  </ListItemIcon>
                  {name}
                </StyledMenuItem>
              )
            })}
          </Menu>
        </React.Fragment>
      )}
    </Root>
  )
}

Actions.defaultProps = {
  children: null,
  onSort: null,
  onSelect: null,
  specialAction: null,
  columns: [],
  options: null,
}

Actions.propTypes = {
  children: PropTypes.element,
  columns: PropTypes.arrayOf(PropTypes.instanceOf(Column)),
  specialAction: PropTypes.element,
  options: PropTypes.arrayOf(PropTypes.instanceOf(ToolbarMenuOption)),
  onSort: PropTypes.func,
  onSelect: PropTypes.func,
}

export default React.memo(Actions)
