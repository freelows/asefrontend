import React from 'react'
import { IconButton } from '@material-ui/core'
import { MoreVert as MoreVertIcon } from '@material-ui/icons'

const MenuButton = ({ handleOnClick }) => {
  return (
    <IconButton
      aria-controls="toolbar-actions-menu"
      aria-haspopup="true"
      aria-label="toolbar actions"
      onClick={handleOnClick}
    >
      <MoreVertIcon />
    </IconButton>
  )
}

export default React.memo(MenuButton)
