import React from 'react'

import PropTypes from 'prop-types'

import { Menu as MuiMenu } from '@material-ui/core'

const Menu = ({ children, anchorEl, handleOnClose }) => {
  return (
    <MuiMenu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      getContentAnchorEl={null}
      id="toolbar-actions-menu"
      open={!!anchorEl}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      onClose={handleOnClose}
    >
      {children}
    </MuiMenu>
  )
}

Menu.propTypes = {
  children: PropTypes.node.isRequired,
  handleOnClose: PropTypes.func.isRequired,
}

export default Menu
