import styled from 'styled-components'
import {MenuItem} from '@material-ui/core'

export const Root = styled.div`
  display: flex;
  align-items: center;

  > *:not(:last-child) {
    margin-right: 5px;
  }
`
export const StyledMenuItem = styled(MenuItem)`
  padding-left: 16px;
  padding-right: 16px;
  padding-top: 6px;
  padding-bottom: 6px;
  display: flex;
  position: relative;
  text-align: left;
  align-items: center;
  justify-content: flex-start;
`
