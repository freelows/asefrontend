import React, { useState } from 'react'
import PropTypes from 'prop-types'

import {
  MenuItem,
  Checkbox,
  ListItemText,
  IconButton,
  Typography,
  ListItemIcon,
} from '@material-ui/core'
import { ViewColumnRounded as ViewColumnIcon } from '@material-ui/icons'
import { Tooltip } from 'components/ui'
import { Column } from 'models'
import StyledMenu from './styles'

const ColumnsViewer = ({ items, onSelect }) => {
  const [anchorEl, setAnchorEl] = useState(null)

  const handleOnClick = (e) => {
    setAnchorEl(e.currentTarget)
  }

  const handleOnClose = () => {
    setAnchorEl(null)
  }

  return (
    <>
      <Tooltip title="Adjust Columns">
        <IconButton
          aria-controls="toolbar-adjust-columns-menu"
          aria-haspopup="true"
          aria-label="Adjust columns"
          onClick={handleOnClick}
        >
          <ViewColumnIcon />
        </IconButton>
      </Tooltip>
      <StyledMenu
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        getContentAnchorEl={null}
        id="toolbar-adjust-columns-menu"
        open={!!anchorEl}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        onClose={handleOnClose}
      >
        <MenuItem disabled value="">
          <Typography color="textPrimary" variant="subtitle1">
            Adjust Columns
          </Typography>
        </MenuItem>
        {items.map(({ name, isVisible, label }) => {
          const handleOnSelect = () => onSelect(name)

          return (
            <MenuItem key={name} value={name} onClick={handleOnSelect}>
              <ListItemIcon>
                <Checkbox disableRipple checked={isVisible} color="primary" />
              </ListItemIcon>
              <ListItemText primary={label} />
            </MenuItem>
          )
        })}
      </StyledMenu>
    </>
  )
}

ColumnsViewer.propTypes = {
  items: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
  onSelect: PropTypes.func.isRequired,
}
export default ColumnsViewer
