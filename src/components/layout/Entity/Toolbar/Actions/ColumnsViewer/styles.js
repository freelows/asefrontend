import styled from 'styled-components'
import { Menu } from '@material-ui/core'

const StyledMenu = styled(Menu)`
  div.MuiPaper-root.MuiMenu-paper {
    max-height: 366px;
  }
`

export default StyledMenu
