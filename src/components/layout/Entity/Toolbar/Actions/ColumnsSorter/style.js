import Styled from 'styled-components'

const StyledSorter = Styled.div`
  padding-right: 10px; 
`
export default StyledSorter
