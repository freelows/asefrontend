import React from 'react'
import {
  InputAdornment,
  MenuItem,
  ListItemIcon,
  TextField,
} from '@material-ui/core'
import PropTypes from 'prop-types'
import {
  ArrowUpward as ArrowUpwardIcon,
  ArrowDownwardRounded as ArrowDownwardIcon,
} from '@material-ui/icons'
import { Column } from 'models'
import StyledSorter from './style'

const ColumnsSorter = ({ items, onSort }) => {
  const currentSort = items.find((item) => item.isSortable)

  return (
    <StyledSorter>
      <TextField
        select
        id="outlined-select-currency"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <ArrowUpwardIcon color="primary" />
            </InputAdornment>
          ),
        }}
        SelectProps={{
          MenuProps: {
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'right',
            },
            transformOrigin: {
              vertical: 'top',
              horizontal: 'right',
            },
            getContentAnchorEl: null,
          },
        }}
        value={currentSort && currentSort.name}
        variant="outlined"
      >
        {items.map(({ name, isSortable, isAsc, label }) => {
          const handleOnClick = () => onSort(name)

          return (
            <MenuItem key={`menu-sort${name}`} onClick={handleOnClick}>
              {isSortable && (
                <ListItemIcon>
                  {isAsc ? (
                    <ArrowUpwardIcon color="primary" />
                  ) : (
                    <ArrowDownwardIcon color="primary" />
                  )}
                </ListItemIcon>
              )}
              {label}
            </MenuItem>
          )
        })}
      </TextField>
    </StyledSorter>
  )
}

ColumnsSorter.propTypes = {
  items: PropTypes.arrayOf(PropTypes.instanceOf(Column)).isRequired,
  onSort: PropTypes.func.isRequired,
}
export default ColumnsSorter
