import React, { useState } from 'react'
import PropTypes from 'prop-types'

import { FormControl, Select, InputLabel } from '@material-ui/core'

import { MenuItem } from 'components/ui'

import Root from './styles'

//TODO: Redistribute it equaly for two or more presets
const CustomPreset = ({
  id,
  label,
  nullOptionLabel,
  selectedValue,
  data,
  onChange,
  width
}) => {
  const [value, setValue] = useState(selectedValue)

  const handleOnChange = ({ target: { value } }) => {
    onChange({ id, value })
    setValue(value)
  }

  return (
    <Root width={width}>
      <FormControl fullWidth>
        <InputLabel shrink htmlFor={`${label}-preset-label`}>
          {label}
        </InputLabel>
        <Select
          displayEmpty
          id={`${label}-preset-select`}
          MenuProps={{
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'left'
            },
            transformOrigin: {
              vertical: 'top',
              horizontal: 'left'
            },
            getContentAnchorEl: null
          }}
          value={value}
          onChange={handleOnChange}
        >
          <MenuItem value="">{nullOptionLabel}</MenuItem>
          {data.map(({ value: dataValue, label }) => (
            <MenuItem key={dataValue} value={dataValue}>
              {label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Root>
  )
}

CustomPreset.defaultProps = {
  label: '',
  selectedValue: ''
}

CustomPreset.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  nullOptionLabel: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
  selectedValue: PropTypes.string,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired
    })
  ).isRequired,
  onChange: PropTypes.func.isRequired
}

export default CustomPreset
