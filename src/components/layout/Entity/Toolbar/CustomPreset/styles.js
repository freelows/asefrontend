import styled from 'styled-components'

const Root = styled.div`
  margin-left: 8px;
  margin-right: 8px;
  min-width: ${props => props.width}px;
`

export default Root