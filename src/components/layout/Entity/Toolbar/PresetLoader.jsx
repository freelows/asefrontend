import React from 'react'

import { Box } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'

const labelStyle = {
  marginTop: '0.3em',
}

const textStyle = {
  marginTop: '0.6em',
  marginBottom: '0',
}

const PresetLoader = () => (
  <>
    <Box width="230px">
      <Skeleton height={8} style={labelStyle} width="40%" />
      <Skeleton height={24} style={textStyle} width="100%" />
    </Box>
  </>
)

export default PresetLoader
