import React from 'react'
import PropTypes from 'prop-types'
import { Column, ToolbarMenuOption, Preset } from 'models'

import { Skeleton } from '@material-ui/lab'
import DatePresets from './DatePresets'
import { Root, Customs, Presets } from './styles'
import Actions from './Actions'
import PresetLoader from './PresetLoader'

const Toolbar = ({
  customs,
  columns,
  pagination,
  options,
  presets,
  onChangeDate,
  onSort,
  onSelect,
  loading,
  specialAction,
}) => {
  return (
    <Root>
      {presets && (
        <Presets>
          {loading ? (
            <PresetLoader />
          ) : (
            <DatePresets {...presets[0]} onChangeDate={onChangeDate} />
          )}
        </Presets>
      )}

      <Customs>{!loading && customs}</Customs>

      <Actions
        columns={columns}
        options={options}
        specialAction={specialAction}
        onSelect={onSelect}
        onSort={onSort}
      >
        {loading ? (
          <Skeleton height="8" variant="text" width="200px" />
        ) : (
          pagination
        )}
      </Actions>
    </Root>
  )
}

Toolbar.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.instanceOf(Column)),
  customs: PropTypes.node,
  loading: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.instanceOf(ToolbarMenuOption)),
  pagination: PropTypes.node,
  presets: PropTypes.arrayOf(PropTypes.shape(Preset)),
  specialAction: PropTypes.element,
  onChangeDate: PropTypes.func,
  onSelect: PropTypes.func,
  onSort: PropTypes.func,
}

Toolbar.defaultProps = {
  customs: null,
  columns: [],
  options: [],
  pagination: null,
  onSort: null,
  onSelect: null,
  loading: false,
  presets: null,
  onChangeDate: null,
  specialAction: null,
}

export default Toolbar
