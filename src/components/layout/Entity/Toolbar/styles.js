import styled from 'styled-components'

export const Root = styled.div`
  display: flex;
  flex-wrap: nowrap;
  padding-bottom: ${props => `${props.theme.spacing(1.5)}px`};
`
export const Presets = styled.div`
  display: flex;
  margin-right: 16px;
  > *:not(:last-child) {
    margin-right: 16px;
  }
`
export const Customs = styled.div`
  display: flex;
  flex: 1;
  > *:not(:last-child) {
    margin-right: 16px;
  }
`
