import React from 'react'
import PropTypes from 'prop-types'

import { Button } from '@material-ui/core'
import { buildDates } from 'utils/datePresets'

import Root from './styles'

const Presets = ({ selectedValue, format, data, onChangeDate }) => {
  const handleOnChangeDate = (preset) => {
    const selectedValue = preset.currentTarget.name
    const dates = buildDates({
      format,
      option: selectedValue,
      from: new Date(),
    })

    onChangeDate({
      selectedValue,
      ...dates,
    })
  }

  return (
    <Root>
      {data &&
        data.map(({ label, value }) => (
          <Button
            key={value}
            className={selectedValue === value ? 'selected' : undefined}
            name={value}
            onClick={handleOnChangeDate}
          >
            {label}
          </Button>
        ))}
    </Root>
  )
}

Presets.propTypes = {
  selectedValue: PropTypes.string.isRequired,
  format: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    })
  ).isRequired,
  onChangeDate: PropTypes.func.isRequired,
}

export default Presets
