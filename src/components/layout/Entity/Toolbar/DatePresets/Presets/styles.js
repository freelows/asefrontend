import styled from 'styled-components'

const Root = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  align-content: space-around;
  width: 100%;
  height: 145px;

  > * {
    margin: 5px;
  }

  .selected {
    color: ${props => props.theme.palette.primary.main};
    pointer-events: none;
  }
`
export default Root
