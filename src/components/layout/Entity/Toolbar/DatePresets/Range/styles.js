import styled from 'styled-components'

const Root = styled.div`
  display: flex;
  padding-bottom: 16px;
  align-items: flex-start;
  justify-content: space-around;
  width: 100%;
  > div {
    width: 190px;
    margin-right: 24px;
  }

  > button {
    margin-top: 10px;
  }
`
export default Root
