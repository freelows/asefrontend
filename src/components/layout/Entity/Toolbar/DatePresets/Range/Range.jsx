import React, { useState } from 'react'
import PropTypes from 'prop-types'
import DateFnsUtils from '@date-io/date-fns'

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers'
import { Button } from '@material-ui/core'

import { buildDates, OptionsTypes } from 'utils/datePresets'
import Root from './styles'

const Range = ({ format, onChangeDate }) => {
  const [from, setFrom] = useState(null)
  const [to, setTo] = useState(null)

  const handleOnChangeDate = () => {
    const selectedValue = OptionsTypes.DateRange
    // TODO: Catch possible error in buildDates and throw log event of ui module
    const dates = buildDates({
      format,
      option: selectedValue,
      from,
      to,
    })

    onChangeDate({
      selectedValue,
      ...dates,
    })
  }

  return (
    <Root>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          autoOk
          disableFuture
          disableToolbar
          emptyLabel="mm/dd/yyyy"
          format="MM/dd/yyyy"
          InputAdornmentProps={{ position: 'start' }}
          inputVariant="outlined"
          label="From"
          value={from}
          variant="inline"
          onChange={date => setFrom(date)}
        />

        <KeyboardDatePicker
          autoOk
          disableFuture
          disableToolbar
          emptyLabel="mm/dd/yyyy"
          format="MM/dd/yyyy"
          InputAdornmentProps={{ position: 'start' }}
          inputVariant="outlined"
          label="To"
          value={to}
          variant="inline"
          onChange={date => setTo(date)}
        />

        <Button
          color="primary"
          disabled={!from || !to || from > to}
          onClick={handleOnChangeDate}
        >
          Apply
        </Button>
      </MuiPickersUtilsProvider>
    </Root>
  )
}

Range.propTypes = {
  format: PropTypes.string.isRequired,
  onChangeDate: PropTypes.func.isRequired,
}

export default Range
