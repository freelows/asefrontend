import styled from 'styled-components'

import { TextField, ExpansionPanel } from '@material-ui/core'

export const StyledTextField = styled(TextField)`
  cursor: pointer;
  * {
    cursor: pointer;
  }
`

export const StyledExpansionPanel = styled(ExpansionPanel)`
  &&.MuiExpansionPanel-root.Mui-expanded {
    margin: 0;
  }
  margin: 0;

  :last-of-type {
    border-top: 1px solid ${props => props.theme.palette.divider};
    margin: 0;
  }
`
