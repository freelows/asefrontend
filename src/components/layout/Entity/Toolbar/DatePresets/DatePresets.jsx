import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Popover,
  InputAdornment,
  Typography,
} from '@material-ui/core'
import {
  ExpandMore as ExpandMoreIcon,
  AccessTimeRounded as AccessTimeIcon,
  ArrowDropDown as ArrowDropDownIcon,
} from '@material-ui/icons'
import { options, OptionsTypes } from 'utils/datePresets'
import Presets from './Presets'
import Range from './Range'

import { StyledTextField, StyledExpansionPanel } from './styles'

const panels = [
  { panelId: 'presets', title: 'Presets', content: Presets },
  { panelId: 'dateRange', title: 'Date Range', content: Range },
]

const presetPanelId = panels[0].panelId

function getDateText(selectedValue) {
  return selectedValue !== OptionsTypes.DateRange
    ? options[selectedValue]
    : 'From - To'
}

const DatePresets = ({ id, selectedValue, format, data, onChangeDate }) => {
  const [inputText, setInputText] = useState('')
  const [anchorEl, setAnchorEl] = useState(null)
  const [expanded, setExpanded] = useState(presetPanelId)

  const openPopover = (e) => setAnchorEl(e.currentTarget)
  const closePopover = () => setAnchorEl(null)

  const onlyPresets =
    data && data.filter((x) => x.value !== OptionsTypes.DateRange)

  const handleOnChangeDate = (dates) => {
    onChangeDate({ id, ...dates })
    closePopover()
  }

  useEffect(() => {
    if (selectedValue) setInputText(getDateText(selectedValue))
  }, [selectedValue])

  return (
    <>
      <StyledTextField
        id="date-preset"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <AccessTimeIcon color="secondary" />
            </InputAdornment>
          ),
          endAdornment: (
            <InputAdornment position="end">
              <ArrowDropDownIcon color="action" />
            </InputAdornment>
          ),
        }}
        label="Time Frame"
        value={inputText}
        onClick={openPopover}
      />

      <Popover
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        id="date-preset-menu"
        open={!!anchorEl}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        onClose={closePopover}
      >
        {panels.map(({ panelId, title, content: Content }) => {
          const onChangePanel = () => setExpanded(panelId)

          const contentProps = {
            selectedValue,
            format,
            data: panelId === presetPanelId ? onlyPresets : undefined,
            onChangeDate: handleOnChangeDate,
          }

          return (
            <StyledExpansionPanel
              key={panelId}
              square
              expanded={panelId === expanded}
              onChange={onChangePanel}
            >
              <ExpansionPanelSummary
                aria-controls={`${panelId}-content"`}
                expandIcon={<ExpandMoreIcon />}
                id={`${panelId}-header`}
              >
                <Typography
                  color={panelId === expanded ? 'textPrimary' : 'textSecondary'}
                  variant="subtitle1"
                >
                  {title}
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Content {...contentProps} />
              </ExpansionPanelDetails>
            </StyledExpansionPanel>
          )
        })}
      </Popover>
    </>
  )
}

DatePresets.propTypes = {
  id: PropTypes.string.isRequired,
  format: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  selectedValue: PropTypes.string,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    })
  ).isRequired,
  onChangeDate: PropTypes.func.isRequired,
}

DatePresets.defaultProps = {
  selectedValue: null,
}

export default DatePresets
