import React, { useRef, useState } from 'react'

import debounce from 'lodash.debounce'

import { InputAdornment, IconButton } from '@material-ui/core'
import { Search as SearchIcon, Cancel as CancelIcon } from '@material-ui/icons'

import PropTypes from 'prop-types'

import StyledTextField from './styles'

const AdvancedSearch = ({ placeholder, onSearch }) => {
  const [searchTerm, setSearchTerm] = useState('')
  const inputRef = useRef(null)

  const handleDebounce = (value) => {
    onSearch(value)
    setSearchTerm(value)
  }

  const debouncedOnChange = debounce(handleDebounce, 500)

  const handleOnChange = (event) => {
    event.persist()
    debouncedOnChange(event.target.value)
  }

  const clearSearchTerm = () => {
    setSearchTerm('')
    debouncedOnChange(null)
    inputRef.current.querySelector('input').value = ''
  }

  return (
    <StyledTextField
      ref={inputRef}
      fullWidth
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon aria-label="search" color="inherit" />
          </InputAdornment>
        ),
        endAdornment: searchTerm && (
          <InputAdornment>
            <IconButton
              aria-label="clear"
              color="inherit"
              onClick={clearSearchTerm}
            >
              <CancelIcon color="inherit" />
            </IconButton>
          </InputAdornment>
        ),
      }}
      margin="normal"
      placeholder={placeholder}
      variant="outlined"
      onChange={handleOnChange}
    />
  )
}

AdvancedSearch.propTypes = {
  placeholder: PropTypes.string.isRequired,
  onSearch: PropTypes.func.isRequired,
}

export default AdvancedSearch
