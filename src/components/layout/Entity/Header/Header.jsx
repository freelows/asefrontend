import React from 'react'

import PropTypes from 'prop-types'
import { Grid, IconButton, Typography } from '@material-ui/core'
import StarBorderIcon from '@material-ui/icons/StarBorderRounded'

import AdvancedSearch from './AdvancedSearch'

import { StyledStarIcon, StyledGrid, IconWrapper } from './styles'

const Header = ({ title, selectedItemId, placeholder, onSave, onSearch }) => {
  const handleOnSave = () => onSave(selectedItemId)
  const icon = selectedItemId ? <StyledStarIcon /> : <StarBorderIcon />

  return (
    <Grid container>
      <StyledGrid container item alignItems="center" xs={12}>
        <Grid item>
          <Typography color="textPrimary" variant="h6">
            {title}
          </Typography>
        </Grid>
        <Grid item>
          {onSave ? (
            <IconButton onClick={handleOnSave}>{icon}</IconButton>
          ) : (
            <IconWrapper>{icon}</IconWrapper>
          )}
        </Grid>
      </StyledGrid>
      <Grid item xs={12}>
        {onSearch && (
          <AdvancedSearch placeholder={placeholder} onSearch={onSearch} />
        )}
      </Grid>
    </Grid>
  )
}

Header.defaultProps = {
  onSave: null,
  onSearch: null,
  selectedItemId: null,
  placeholder: 'Search',
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  selectedItemId: PropTypes.string,
  onSave: PropTypes.func,
  onSearch: PropTypes.func,
}

export default Header
