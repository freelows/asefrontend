import styled from 'styled-components'

import StarIcon from '@material-ui/icons/StarRounded'
import { Grid } from '@material-ui/core'

export const IconWrapper = styled.div`
  display: flex;
  padding: 14px;
`

export const StyledStarIcon = styled(StarIcon)`
  color: gold;
`

export const StyledGrid = styled(Grid)`
  padding-bottom: 6px;
`
