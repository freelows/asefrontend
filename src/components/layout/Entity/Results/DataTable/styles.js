import styled from 'styled-components'

import { TableCell, TableHead } from '@material-ui/core'

export const StyledTableCell = styled(TableCell)`
  min-width: 170px;
`

export const SortDescription = styled.span`
  border: 0;
  clip: rect(0 0 0 0);
  height: 1;
  margin: -1;
  overflow: hidden;
  padding: 0;
  position: absolute;
  top: 20;
  width: 1;
`
export const StyledTableHead = styled(TableHead)`
  background-color: ${(props) =>
    props.theme.palette.dataTable.headerBackground};
`
