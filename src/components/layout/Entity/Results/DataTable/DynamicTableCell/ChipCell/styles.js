import styled from 'styled-components'

export default styled.div`
  background-color: ${props => props.color};
  color: ${props => props.textColor};
  display: inline-block;
  padding: 4px 12px;
  border-radius: 4px;
`
