import React from 'react'
import PropTypes from 'prop-types'

import { Typography } from '@material-ui/core'

import StyledDiv from './styles'

const ChipCell = props => {
  const { value, color, textColor } = props

  return (
    <StyledDiv color={color} textColor={textColor}>
      <Typography variant="body2">{value}</Typography>
    </StyledDiv>
  )
}

ChipCell.defaultProps = {
  color: 'primary',
  textColor: 'secondary'
}

ChipCell.propTypes = {
  value: PropTypes.string.isRequired,
  color: PropTypes.string,
  textColor: PropTypes.string
}

export default React.memo(ChipCell)
