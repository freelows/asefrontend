import React from 'react'
import PropTypes from 'prop-types'

import formatPhoneNumber from 'utils/formatPhoneNumber'

const PhoneCell = (props) => {
  const { value } = props

  return <> {formatPhoneNumber(value)} </>
}

PhoneCell.propTypes = {
  value: PropTypes.string.isRequired,
}

export default React.memo(PhoneCell)
