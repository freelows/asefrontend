import React from 'react'
import PropTypes from 'prop-types'

import Tooltip from 'components/ui/Tooltip'

import willTruncateText from 'utils/willTruncateText'

import MuiTypography from '@material-ui/core/Typography'

import StyledTruncateDiv from './styles'

const SingleText = ({ value, truncateText, maxWidth, variant }) => {
  return willTruncateText(value, maxWidth) && truncateText ? (
    <Tooltip title={value}>
      <MuiTypography component="span" variant={variant}>
        <StyledTruncateDiv>{value}</StyledTruncateDiv>
      </MuiTypography>
    </Tooltip>
  ) : (
    <MuiTypography variant={variant}>{value}</MuiTypography>
  )
}

SingleText.defaultProps = {
  truncateText: false,
  maxWidth: 250,
  variant: 'body2',
  value: '',
}

SingleText.propTypes = {
  value: PropTypes.string,
  truncateText: PropTypes.bool,
  maxWidth: PropTypes.number,
  variant: PropTypes.oneOf([
    'body1',
    'body2',
    'caption',
    'button',
    'overline',
    'inherit',
  ]),
}

export default React.memo(SingleText)
