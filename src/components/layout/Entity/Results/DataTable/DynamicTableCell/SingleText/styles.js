import styled from 'styled-components'

export default styled.span`
  display: block;
  max-width: ${props => props.maxWidth}px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`
