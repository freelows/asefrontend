import React from 'react'
import PropTypes from 'prop-types'

import SingleText from '../SingleText'

const MultiLine = ({
  value,
  truncateText,
  maxWidth,
  secondaryText,
  textProps,
}) => {
  const { secondaryTextColor } = textProps

  return (
    <>
      <SingleText
        maxWidth={maxWidth}
        text={value}
        truncateText={truncateText}
        variant="body1"
      />
      {secondaryText && (
        <SingleText
          color={secondaryTextColor}
          maxWidth={maxWidth}
          text={secondaryText}
          truncateText={truncateText}
          variant="body2"
        />
      )}
    </>
  )
}

MultiLine.defaultProps = {
  truncateText: false,
  maxWidth: 250,
  secondaryText: '',
  textProps: {
    secondaryTextColor: 'textSecondary',
  },
}

MultiLine.propTypes = {
  value: PropTypes.string.isRequired,
  secondaryText: PropTypes.string,
  truncateText: PropTypes.bool,
  maxWidth: PropTypes.number,
  textProps: PropTypes.shape({
    secondaryTextColor: PropTypes.oneOf([
      'inherit',
      'primary',
      'secondary',
      'textPrimary',
      'textSecondary',
      'error',
    ]),
  }),
}

export default React.memo(MultiLine)
