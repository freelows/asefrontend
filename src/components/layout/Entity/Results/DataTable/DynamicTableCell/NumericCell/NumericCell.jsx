import React from 'react'
import PropTypes from 'prop-types'

import MuiTypography from '@material-ui/core/Typography'

const NumericCell = ({ value, variant }) => {
  return <MuiTypography variant={variant}>{value}</MuiTypography>
}

NumericCell.defaultProps = {
  variant: 'body2'
}

NumericCell.propTypes = {
  value: PropTypes.number.isRequired,
  variant: PropTypes.oneOf([
    'body1',
    'body2',
    'caption',
    'button',
    'overline',
    'inherit'
  ])
}

export default React.memo(NumericCell)
