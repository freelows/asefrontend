import styled from 'styled-components'

import { TableCell } from '@material-ui/core'

export default styled(TableCell)`
  max-width: ${props => props.maxwidth}px;
`
