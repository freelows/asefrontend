import React from 'react'
import PropTypes from 'prop-types'

import { format, formatDistanceToNow } from 'date-fns'
import MultipleLine from '../MultipleLine'

const DateCell = props => {
  const { value, formatString, useRelativeDate } = props
  const isValidDate = !isNaN(Date.parse(value))

  if (!isValidDate) {
    // TODO: Use react error boundaries to handle this
    // eslint-disable-next-line no-console
    console.error(`Provided date is incorrect. Date value: ${value}`)
    return null
  }

  const date = new Date(value)
  const formatedWithDistance = formatDistanceToNow(date, { addSuffix: true })
  const formatted = format(date, formatString)

  return useRelativeDate ? (
    <MultipleLine
      truncateText
      secondaryText={formatted}
      text={formatedWithDistance}
    />
  ) : (
    formatted
  )
}

DateCell.defaultProps = {
  formatString: 'yyyy-MM-dd hh:mm:ss aa',
  useRelativeDate: false
}

DateCell.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)])
    .isRequired,
  formatString: PropTypes.string,
  useRelativeDate: PropTypes.bool
}

export default React.memo(DateCell)
