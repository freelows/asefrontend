import React from 'react'
import PropTypes from 'prop-types'

import { Button } from '@material-ui/core'

const ActionCell = ({ value, color, action }) => (
  <Button color={color} label={value} variant="outlined" onClick={action}>
    {value}
  </Button>
)

ActionCell.propTypes = {
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary'])
    .isRequired,
  value: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired
}

export default React.memo(ActionCell)
