import React, { Suspense } from 'react'
import PropTypes from 'prop-types'

import { TableCellType } from 'models'
import convertValue from 'utils/convertValue'
import maskValue from 'utils/maskValue'
import StyledTableCell from './styles'

import ActionCell from './ActionCell'
import ChipCell from './ChipCell'
import DateCell from './DateCell'
import MultipleLine from './MultipleLine'
import PhoneCell from './PhoneCell'
import NumericCell from './NumericCell'
import SingleText from './SingleText'

const CELL_DICTIONARY = {
  [TableCellType.Action]: ActionCell,
  [TableCellType.Chip]: ChipCell,
  [TableCellType.Date]: DateCell,
  [TableCellType.MultipleLine]: MultipleLine,
  [TableCellType.Phone]: PhoneCell,
  [TableCellType.SingleText]: SingleText,
  [TableCellType.Numeric]: NumericCell,
}

const DynamicTableCell = ({ textProps, align, value, secondaryText, mask }) => {
  const { type, maxWidth, maskable } = textProps

  const TableCellContent = CELL_DICTIONARY[type]

  const convertedValue = convertValue(value, type)
  const maskedValue =
    mask && maskable ? maskValue(convertedValue, type) : convertedValue

  return (
    <Suspense fallback={<td />}>
      <StyledTableCell align={align} maxwidth={maxWidth}>
        <TableCellContent
          {...textProps}
          secondaryText={secondaryText}
          value={maskedValue}
        />
      </StyledTableCell>
    </Suspense>
  )
}

DynamicTableCell.defaultProps = {
  align: 'right',
  value: '',
  secondaryText: '',
  mask: false,
}

DynamicTableCell.propTypes = {
  align: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date),
  ]),
  secondaryText: PropTypes.string,
  mask: PropTypes.bool,
  textProps: PropTypes.shape({
    type: PropTypes.oneOf(Object.values(TableCellType)).isRequired,
    lines: PropTypes.number,
    primaryColor: PropTypes.string,
    action: PropTypes.func,
    maxWidth: PropTypes.number,
    maskable: PropTypes.bool,
  }).isRequired,
}

export default React.memo(DynamicTableCell)
