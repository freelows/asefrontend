import React from 'react'
import PropTypes from 'prop-types'

import { EmptySection } from 'components/ui'
import SearchIcon from '@material-ui/icons/SearchTwoTone'
import { Column } from 'models'

import { Table, TableRow, TableBody, TableSortLabel } from '@material-ui/core'

import DynamicTableCell from './DynamicTableCell'

import { StyledTableHead, StyledTableCell, SortDescription } from './styles'

const DataTable = ({
  columns,
  rows,
  sort: { by: sortBy, isAsc },
  onSelectRow,
  onSort,
  mask,
}) => {
  const sortOrientation = isAsc ? 'asc' : 'desc'
  return rows.length ? (
    <Table aria-label="results table">
      <StyledTableHead>
        <TableRow>
          {columns.map((column) => {
            const handleOnSort = () => {
              onSort({
                by: column.name,
                isAsc: sortBy === column.name ? !isAsc : false,
              })
            }

            return (
              <StyledTableCell
                key={column.name}
                align={column.align}
                sortDirection={column.name === sortBy ? sortOrientation : false}
              >
                {column.isSortable ? (
                  <TableSortLabel
                    active={column.name === sortBy}
                    direction={sortOrientation}
                    onClick={handleOnSort}
                  >
                    {column.label}
                    {column.name === sortBy ? (
                      <SortDescription>
                        {isAsc ? 'sorted ascending' : 'sorted descending'}
                      </SortDescription>
                    ) : null}
                  </TableSortLabel>
                ) : (
                  column.label
                )}
              </StyledTableCell>
            )
          })}
        </TableRow>
      </StyledTableHead>
      <TableBody>
        {rows.map((row) => {
          const handleOnSelectRow = () => onSelectRow(row.id)

          return (
            <TableRow
              key={row.id}
              hover
              role="checkbox"
              tabIndex={-1}
              onClick={handleOnSelectRow}
            >
              {columns.map((column) => {
                const value = row[column.name]

                let cellValue = value
                let secondaryText
                if (Array.isArray(value)) {
                  cellValue = value.shift()
                  secondaryText = value.shift()
                }

                const CustomCell = column.customCell

                return CustomCell ? (
                  <CustomCell key={column.name} column={column} row={row} />
                ) : (
                  <DynamicTableCell
                    key={column.name}
                    align={column.align}
                    cellId={column.name}
                    mask={mask}
                    secondaryText={secondaryText}
                    textProps={column.cell}
                    value={cellValue}
                  />
                )
              })}
            </TableRow>
          )
        })}
      </TableBody>
    </Table>
  ) : (
    <EmptySection
      description="Modifica los filtros ingresados e intenta nuevamente"
      icon={SearchIcon}
      title="No hay resultados que mostrar."
    />
  )
}

DataTable.defaultProps = {
  rows: [],
  mask: false,
}

DataTable.propTypes = {
  columns: PropTypes.arrayOf(Column).isRequired,
  mask: PropTypes.bool,
  rows: PropTypes.arrayOf(Object),
  sort: PropTypes.shape({
    by: PropTypes.string.isRequired,
    isAsc: PropTypes.bool.isRequired,
  }).isRequired,
  onSelectRow: PropTypes.func.isRequired,
  onSort: PropTypes.func.isRequired,
}

export default React.memo(DataTable)
