import React from 'react'
import PropTypes from 'prop-types'

import { Column } from 'models'
import DataTable from './DataTable'
import Loader from './Loader'

import DataTableWrapper from './styles'

const Results = ({
  loading,
  columns,
  sort,
  results,
  pagination,
  onSelectRow,
  onSort,
  mask,
}) => {
  return loading ? (
    <Loader />
  ) : (
    <>
      <DataTableWrapper>
        <DataTable
          columns={columns}
          mask={mask}
          rows={results}
          sort={sort}
          onSelectRow={onSelectRow}
          onSort={onSort}
        />
      </DataTableWrapper>
      {results && results.length > 0 && pagination}
    </>
  )
}

Results.defaultProps = {
  results: [],
  pagination: null,
  loading: false,
  mask: false,
}

Results.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.instanceOf(Object)).isRequired,
  loading: PropTypes.bool,
  mask: PropTypes.bool,
  pagination: PropTypes.node,
  results: PropTypes.arrayOf(Object),
  sort: PropTypes.shape({
    by: PropTypes.string.isRequired,
    isAsc: PropTypes.bool.isRequired,
  }).isRequired,
  onSelectRow: PropTypes.func.isRequired,
  onSort: PropTypes.func.isRequired,
}

export default React.memo(Results)
