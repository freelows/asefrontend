import React from 'react'

import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import styled from 'styled-components'

const cols = Array.from('12345')

const StyledTableHead = styled(TableHead)`
  background-color: ${(props) =>
    props.theme.palette.dataTable.headerBackground};
`

const StyledSkeleton = styled(Skeleton)`
  margin-bottom: 0.5em;
  margin-top: 0.5em;
`

const Loader = () => (
  <Table>
    <StyledTableHead>
      <TableRow>
        {cols.map((col) => (
          <TableCell key={col} align="left">
            <StyledSkeleton height={10} variant="text" width="60%" />
          </TableCell>
        ))}
      </TableRow>
    </StyledTableHead>
    <TableBody>
      {[0, 1].map((row) => (
        <TableRow key={row}>
          {cols.map((col) => (
            <TableCell key={col} align="left">
              <Skeleton variant="text" width="85%" />
            </TableCell>
          ))}
        </TableRow>
      ))}
    </TableBody>
  </Table>
)

export default Loader
