import styled from 'styled-components'

export default styled.div`
  overflow-x: auto;
  width: 100%;
  flex: 1;
`
