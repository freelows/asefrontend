import React from 'react'
import PropTypes from 'prop-types'

import IconButton from '@material-ui/core/IconButton'
import {
  FirstPage as FirstPageIcon,
  KeyboardArrowLeft as KeyboardArrowLeftIcon,
  KeyboardArrowRight as KeyboardArrowRightIcon,
  LastPage as LastPageIcon,
} from '@material-ui/icons'

const Actions = ({ isCompact, count, page, rowsPerPage, onChangePage }) => {
  const goFirst = (event) => onChangePage(event, 0)
  const goPrevious = (event) => onChangePage(event, page - 1)
  const goNext = (event) => onChangePage(event, page + 1)
  const goLast = (event) =>
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1))

  const isDisabledNextPage = page >= Math.ceil(count / rowsPerPage) - 1
  const isDisabledBackPage = page === 0

  return (
    <>
      {!isCompact && (
        <IconButton
          aria-label="first page"
          disabled={isDisabledBackPage}
          onClick={goFirst}
        >
          <FirstPageIcon fontSize="small" />
        </IconButton>
      )}
      <IconButton
        aria-label="previous page"
        disabled={isDisabledBackPage}
        onClick={goPrevious}
      >
        <KeyboardArrowLeftIcon fontSize="small" />
      </IconButton>
      <IconButton
        aria-label="next page"
        disabled={isDisabledNextPage}
        onClick={goNext}
      >
        <KeyboardArrowRightIcon fontSize="small" />
      </IconButton>
      {!isCompact && (
        <IconButton
          aria-label="last page"
          disabled={isDisabledNextPage}
          onClick={goLast}
        >
          <LastPageIcon fontSize="small" />
        </IconButton>
      )}
    </>
  )
}

Actions.defaultProps = {
  isCompact: false,
}

Actions.propTypes = {
  isCompact: PropTypes.bool,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
}

export default Actions
