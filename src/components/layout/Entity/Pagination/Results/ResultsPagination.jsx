import React from 'react'
import PropTypes from 'prop-types'
import { TablePagination } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Actions from '../Actions'

const ResultsPagination = ({
  rowsTotal,
  rowsPerPageOptions,
  page,
  rowsName,
  rowsPerPage,
  onChangePage,
  onChangeRowsPerPage,
}) => {
  const customLabelDisplayedRows = () => ''
  const customLabelRowsPerPage = `${rowsName}s por página:`

  const StyledTablePagination = withStyles({
    menuItem: {
      width: '100%',
      display: 'flex',
      position: 'relative',
      'box-sizing': 'border-box',
      'text-align': 'left',
      'align-items': 'center',
      'padding-top': '10px',
      'padding-bottom': '10px',
      'padding-left': '16px',
      'padding-right': '16px',
      'justify-content': 'flex-start',
      'text-decoration': 'none',
    },
  })(TablePagination)

  return (
    <StyledTablePagination
      ActionsComponent={Actions}
      component="div"
      count={rowsTotal}
      labelDisplayedRows={customLabelDisplayedRows}
      labelRowsPerPage={customLabelRowsPerPage}
      page={page}
      rowsPerPage={rowsPerPage}
      rowsPerPageOptions={rowsPerPageOptions}
      SelectProps={{
        inputProps: { 'aria-label': customLabelRowsPerPage },
      }}
      onChangePage={onChangePage}
      onChangeRowsPerPage={onChangeRowsPerPage}
    />
  )
}

ResultsPagination.propTypes = {
  rowsTotal: PropTypes.number.isRequired,
  rowsPerPageOptions: PropTypes.arrayOf(PropTypes.number).isRequired,
  page: PropTypes.number.isRequired,
  rowsName: PropTypes.string.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  onChangeRowsPerPage: PropTypes.func,
}

ResultsPagination.defaultProps = {
  onChangeRowsPerPage: null,
}

export default ResultsPagination
