import styled from 'styled-components'

import { TablePagination } from '@material-ui/core'

export default styled(TablePagination)`
  .MuiTablePagination-menuItem {
    color: red;
    width: 100%;
    display: flex;
    position: relative;
    box-sizing: border-box;
    text-align: left;
    align-items: center;
    padding-top: 8px;
    padding-bottom: 8px;
    justify-content: flex-start;
    text-decoration: none;
  }

  &.MuiTablePagination-menuItem {
    color: red;
    width: 100%;
    display: flex;
    position: relative;
    box-sizing: border-box;
    text-align: left;
    align-items: center;
    padding-top: 8px;
    padding-bottom: 8px;
    justify-content: flex-start;
    text-decoration: none;
  }
`
