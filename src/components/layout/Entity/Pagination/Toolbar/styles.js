import styled from 'styled-components'

export const Root = styled.div`
  margin-left: 20px;
`
export const Caption = styled.span`
  font-size: 0.75rem;
  line-height: 1.66;
  letter-spacing: 0.03333em;
  margin-right: ${(props) => `${props.theme.spacing(1)}px`};
`
