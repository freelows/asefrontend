import React from 'react'
import PropTypes from 'prop-types'

import Actions from '../Actions'
import { Caption, Root } from './styles'

const ToolbarPagination = ({
  page,
  rowsPerPage,
  rowsTotal,
  rowsName,
  onChangePage,
}) => {
  const from = rowsTotal === 0 ? 0 : page * rowsPerPage + 1
  const to = Math.min(rowsTotal, (page + 1) * rowsPerPage)

  return (
    <Root>
      <Caption>
        <span>
          <b>{from}</b>
          <span> - </span>
          <b>{to}</b>
          <span> of </span>
          <b>{rowsTotal}</b>
          <span>{`  ${rowsName}s`}</span>
        </span>
      </Caption>
      <Actions
        isCompact
        count={rowsTotal}
        page={page}
        rowsPerPage={rowsPerPage}
        onChangePage={onChangePage}
      />
    </Root>
  )
}

ToolbarPagination.propTypes = {
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  rowsTotal: PropTypes.number.isRequired,
  rowsName: PropTypes.string.isRequired,
  onChangePage: PropTypes.func.isRequired,
}

export default ToolbarPagination
