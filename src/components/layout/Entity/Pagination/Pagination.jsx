import React from 'react'
import PropTypes from 'prop-types'

import ResultsPagination from './Results'
import ToolbarPagination from './Toolbar'

const Pagination = ({
  location,
  rowsTotal,
  rowsPerPageOptions,
  rowsName,
  page,
  rowsPerPage,
  onChangePage,
  onChangeRowsPerPage,
}) => {
  const handleOnChangePage = (e, page) => onChangePage(page)
  const handleOnChangeRowsPerPage = (e) => onChangeRowsPerPage(e.target.value)

  return location === 'bottom' ? (
    <ResultsPagination
      page={page}
      rowsName={rowsName}
      rowsPerPage={rowsPerPage}
      rowsPerPageOptions={rowsPerPageOptions}
      rowsTotal={rowsTotal}
      onChangePage={handleOnChangePage}
      onChangeRowsPerPage={handleOnChangeRowsPerPage}
    />
  ) : (
    <ToolbarPagination
      page={page}
      rowsName={rowsName}
      rowsPerPage={rowsPerPage}
      rowsTotal={rowsTotal}
      onChangePage={handleOnChangePage}
    />
  )
}

Pagination.defaultProps = {
  onChangeRowsPerPage: null,
  rowsPerPageOptions: [],
}

Pagination.propTypes = {
  location: PropTypes.oneOf(['bottom', 'top']).isRequired,
  rowsTotal: PropTypes.number.isRequired,
  rowsPerPageOptions: PropTypes.arrayOf(PropTypes.number),
  rowsName: PropTypes.string.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  onChangeRowsPerPage: PropTypes.func,
}

export default React.memo(Pagination)
