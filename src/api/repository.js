import axios from 'axios'

class Repository {
  get({ url, params }) {
    return axios
      .get(`${process.env.REACT_APP_BASE_URL}${url}`, params)
      .then((response) => {
        console.log('response', response)
        return response.data
      })
  }

  post({ url, payload }) {
    return axios
      .post(`${process.env.REACT_APP_BASE_URL}${url}`, payload)
      .then((response) => {
        console.log('response post', response)
        return response.data
      })
  }
}
export default Repository
