import Financieros from './financieros'
import Inspecciones from './inspecciones'

const repositories = {
  financieros: new Financieros(),
  inspecciones: new Inspecciones(),
}

const RepositoryFactory = {
  get: (name) => repositories[name],
}

export default RepositoryFactory
