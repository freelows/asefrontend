import RepositoryFactory from './repositoryFactory'

export const financierosRepository = RepositoryFactory.get('financieros')
export const inspeccionesRepository = RepositoryFactory.get('inspecciones')
