import Repository from './repository'

class Inspecciones extends Repository {
  get resource() {
    return 'Inspecciones'
  }

  async find() {
    return await this.get({ url: 'api/inspecciones' })
  }

  save(payload) {
    return this.post({ url: 'api/inspecciones', payload })
  }
}

export default Inspecciones
