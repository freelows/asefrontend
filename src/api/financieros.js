import { Financiero } from 'models'
import Repository from './repository'

class Financieros extends Repository {
  get resource() {
    return 'Financieros'
  }

  async find() {
    return await this.get({ url: 'api/financieros' })
  }

  save(payload) {
    return this.post({ url: 'api/financieros', payload })
  }
}

export default Financieros
