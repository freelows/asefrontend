import { combineReducers } from 'redux'
import { alerts, ui, favorites, filters, results } from './modules'

export default combineReducers({
  [alerts.namespace]: alerts.reducer,
  [ui.namespace]: ui.reducer,
  [favorites.namespace]: favorites.reducer,
  [filters.namespace]: filters.reducer,
  [results.namespace]: results.reducer,
})
