import { put, call, select, takeLatest, all } from 'redux-saga/effects'

import { financierosRepository, inspeccionesRepository } from 'api'

export function* filterFinancieros() {
  const filteredSchedules = yield call([
    financierosRepository,
    financierosRepository.find,
  ])

  return filteredSchedules
}

export function* filterInspecciones() {
  const filteredSchedules = yield call([
    inspeccionesRepository,
    inspeccionesRepository.find,
  ])

  return filteredSchedules
}
