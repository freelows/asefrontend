import { call, put, select, takeLatest } from 'redux-saga/effects'

import { filters, favorites, ui } from 'store/modules'

export function* selectFavorite(action) {
  yield put(favorites.actions.setSelected(action.payload.id))

  const selectedFavorite = yield select(favorites.selectors.getSelectedItem)
  yield put(
    filters.actions.set({
      items: selectedFavorite.filters,
      normalizeKey: 'id',
    })
  )
}

export function* save({ payload: { name, description, owner, createdBy } }) {
  try {
    const allFilters = yield select(filters.selectors.getAll)

    const payload = {
      createdBy,
      description,
      name,
      owner,
      filters: allFilters,
    }

    yield put(
      ui.actions.showAlert({
        message: `Favorite has been saved.`,
      })
    )
  } catch (error) {
    yield put(
      ui.actions.showAlert({
        error,
        message: `Oops!, something went wrong saving Favorite.`,
      })
    )
  }
}

const favoritesSaga = [
  takeLatest(favorites.actionTypes.SELECT, selectFavorite),
  takeLatest(favorites.actionTypes.SAVE, save),
]

export default favoritesSaga
