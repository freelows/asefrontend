import { all, put, call, select, takeLatest } from 'redux-saga/effects'

import { filters, results, ui } from 'store/modules'
import { filterFinancieros, filterInspecciones } from './api'

export const defaultAdvancedFilter = {
  field: { path: 'Id', type: 'guid' },
  operator: {
    id: 'eq',
    query: '[path] eq [value]',
  },
  value: null,
}

const getResults = {
  Financiero: filterFinancieros,
  Inspeccione: filterInspecciones,
}

export function* getFilters() {
  const allFilters = yield all([
    select(filters.selectors.getPreset),
    select(filters.selectors.getDefault),
    select(filters.selectors.getAll),
    select(filters.selectors.getCustomPresets),
  ])

  return allFilters
}

export function* applyFilters() {
  const entity = yield select(ui.selectors.getEntity)
  try {
    yield put(results.actions.fetching())

    const filteredSchedules = yield call(getResults[entity])
    console.log('filteredSchedules', filteredSchedules)
    yield put(
      results.actions.set({
        items: filteredSchedules,
        total: filteredSchedules.length,
      })
    )
  } catch (error) {
    yield put(
      ui.actions.showAlert({
        error,
        message: `Oops!, something went wrong filtering ${entity}.`,
      })
    )
  } finally {
    yield put(results.actions.fetched())
  }
}

export function* setUpConfiguration() {
  try {
    yield put(filters.actions.apply())
  } catch (error) {
    yield put(
      ui.actions.showAlert({
        error,
        message: `Oops!, something went wrong loading configuration.`,
      })
    )
  } finally {
    yield put(filters.actions.fetched())
  }
}

const filtersSaga = [
  takeLatest(filters.actionTypes.APPLY, applyFilters),
  takeLatest(filters.actionTypes.FETCH_CONFIGURATION, setUpConfiguration),
]

export default filtersSaga
