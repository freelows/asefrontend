import { filters, results, favorites } from 'store/modules'

const triggerActions = [
  filters.actionTypes.SET,
  filters.actionTypes.ADD,
  filters.actionTypes.REMOVE,
  filters.actionTypes.CLEAR,
  filters.actionTypes.UPDATE_PRESET,
  filters.actionTypes.UPDATE_ADVANCED,
  filters.actionTypes.UPDATE_CUSTOM_PRESET,
  results.actionTypes.UPDATE_SORT,
  results.actionTypes.UPDATE_PAGE,
  results.actionTypes.UPDATE_ROWS_PER_PAGE,
]

const filtersMiddleware = ({ dispatch }) => (next) => (action) => {
  next(action)

  if (
    triggerActions.includes(action.type) &&
    (!action.meta || !action.meta.normalizeKey)
  ) {
    if (
      action.type === filters.actionTypes.CLEAR ||
      action.type === filters.actionTypes.ADD ||
      action.type === filters.actionTypes.REMOVE
    )
      dispatch(favorites.actions.setSelected(null))

    if (action.type !== results.actionTypes.UPDATE_PAGE)
      dispatch(results.actions.resetPage())

    dispatch(filters.actions.apply())
  }
}

export default filtersMiddleware
