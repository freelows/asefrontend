import { all } from 'redux-saga/effects'
import { filtersSaga, favoritesSaga, uiSaga } from './sagas'

export default function* rootSaga() {
  yield all([...filtersSaga, ...favoritesSaga, ...uiSaga])
}
