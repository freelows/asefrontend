import { createSelector } from 'reselect'

const NAME = 'filters'

const actionTypes = {
  FETCHING: `${NAME}/FETCHING`,
  FETCHED: `${NAME}/FETCHED`,
  FETCH_CONFIGURATION: `${NAME}/FETCH_CONFIGURATION`,
  SET: `${NAME}/SET`,
  SET_DEFAULT: `${NAME}/SET_DEFAULT`,
  SET_PRESET: `${NAME}/SET_PRESET`,
  SET_CUSTOM_PRESETS: `${NAME}/SET_CUSTOM_PRESETS`,
  UPDATE_PRESET: `${NAME}/UPDATE_PRESET`,
  UPDATE_CUSTOM_PRESET: `${NAME}/UPDATE_CUSTOM_PRESET`,
  SET_ADVANCED: `${NAME}/SET_ADVANCED`,
  UPDATE_ADVANCED: `${NAME}/UPDATE_ADVANCED`,
  SET_CONFIGURATION: `${NAME}/SET_CONFIGURATION`,
  APPLY: `${NAME}/APPLY`,
  ADD: `${NAME}/ADD`,
  REMOVE: `${NAME}/REMOVE`,
  CLEAR: `${NAME}/CLEAR`,
  RESET: `${NAME}/RESET`,
}

const actions = {
  fetching: () => ({
    type: actionTypes.FETCHING,
  }),
  fetched: () => ({
    type: actionTypes.FETCHED,
  }),
  fetchConfiguration: entity => ({
    type: actionTypes.FETCH_CONFIGURATION,
    payload: { entity },
  }),
  set: ({ items, normalizeKey }) => ({
    type: actionTypes.SET,
    payload: items,
    meta: { normalizeKey, feature: NAME },
  }),
  setDefault: filters => ({
    type: actionTypes.SET_DEFAULT,
    payload: { filters },
  }),
  setPreset: presets => ({
    type: actionTypes.SET_PRESET,
    payload: { presets },
  }),
  setCustomPresets: customPresets => ({
    type: actionTypes.SET_CUSTOM_PRESETS,
    payload: { customPresets },
  }),
  updatePreset: ({ id, selectedValue, from, to }) => ({
    type: actionTypes.UPDATE_PRESET,
    payload: { id, selectedValue, from, to },
  }),
  updateCustomPreset: ({ id, value }) => ({
    type: actionTypes.UPDATE_CUSTOM_PRESET,
    payload: { id, value },
  }),
  setAdvanced: filter => ({
    type: actionTypes.SET_ADVANCED,
    payload: { filter },
  }),
  updateAdvanced: value => ({
    type: actionTypes.UPDATE_ADVANCED,
    payload: { value },
  }),
  setConfiguration: ({ fields, fieldTypes, operators, customFields }) => ({
    type: actionTypes.SET_CONFIGURATION,
    payload: {
      configuration: {
        fields,
        fieldTypes,
        operators,
        customFields,
      },
    },
  }),
  apply: () => ({
    type: actionTypes.APPLY,
  }),
  add: item => ({
    type: actionTypes.ADD,
    payload: {
      item,
    },
  }),
  remove: id => ({
    type: actionTypes.REMOVE,
    payload: {
      id,
    },
  }),
  clear: () => ({
    type: actionTypes.CLEAR,
  }),
  reset: () => ({
    type: actionTypes.RESET,
  }),
}

export const initialState = {
  fetching: true,
  items: null,
  default: [],
  presets: [],
  customPresets: [],
  advanced: null,
  configuration: {
    fields: [],
    fieldTypes: [],
    operators: [],
    customFields: [],
  },
}

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionTypes.FETCHING:
      return { ...state, fetching: true }

    case actionTypes.FETCHED:
      return { ...state, fetching: false }

    case actionTypes.SET:
      return { ...state, items: payload }

    case actionTypes.SET_PRESET:
      return { ...state, presets: payload.presets }

    case actionTypes.SET_CUSTOM_PRESETS:
      return { ...state, customPresets: payload.customPresets }

    case actionTypes.UPDATE_PRESET: {
      const presets = state.presets.map(preset => {
        if (preset.id !== payload.id) return preset

        return {
          ...preset,
          selectedValue: payload.selectedValue,
          filter: {
            ...preset.filter,
            value: payload.from,
            value2: payload.to,
          },
        }
      })

      return {
        ...state,
        presets,
      }
    }

    case actionTypes.UPDATE_CUSTOM_PRESET: {
      const customPresets = state.customPresets.map(customPreset => {
        if (customPreset.id !== payload.id) return customPreset

        return {
          ...customPreset,
          filter: { ...customPreset.filter, value: payload.value },
        }
      })

      return {
        ...state,
        customPresets,
      }
    }

    case actionTypes.SET_DEFAULT:
      return { ...state, default: payload.filters }

    case actionTypes.SET_ADVANCED:
      return { ...state, advanced: payload.filter }

    case actionTypes.UPDATE_ADVANCED:
      return { ...state, advanced: { ...state.advanced, value: payload.value } }

    case actionTypes.SET_CONFIGURATION:
      return { ...state, configuration: payload.configuration }

    case actionTypes.ADD:
      return {
        ...state,
        items: { ...state.items, [payload.item.id]: payload.item },
      }

    case actionTypes.REMOVE: {
      const { [payload.id]: _, ...restOfItems } = state.items

      return {
        ...state,
        items: Object.keys(restOfItems).length ? restOfItems : null,
      }
    }

    case actionTypes.CLEAR:
      return {
        ...state,
        items: null,
      }

    case actionTypes.RESET:
      return initialState

    default:
      return state
  }
}

const getFetching = state => state[NAME].fetching
const getItems = state => state[NAME].items
const getFields = state => state[NAME].configuration.fields
const getFieldTypes = state => state[NAME].configuration.fieldTypes
const getOperators = state => state[NAME].configuration.operators
const getCustomFields = state => state[NAME].configuration.customFields
const getAdvanced = state => state[NAME].advanced
const getPreset = state => state[NAME].presets
const getCustomPresets = state => state[NAME].customPresets
const getDefault = state => state[NAME].default

const getAll = createSelector([getItems], items =>
  items ? Object.values(items) : [],
)

const getHasItems = createSelector([getAll], items => items.length > 0)

const getAdvancedFilter = createSelector([getAdvanced], advanced =>
  advanced && advanced.value ? advanced : null,
)

const selectors = {
  getFetching,
  getFields,
  getFieldTypes,
  getOperators,
  getCustomFields,
  getAll,
  getAdvancedFilter,
  getPreset,
  getCustomPresets,
  getDefault,
  getHasItems,
}

const filters = {
  namespace: NAME,
  initialState,
  actionTypes,
  actions,
  reducer,
  selectors,
}

export default filters
