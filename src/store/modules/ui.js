const NAME = 'ui'

const actionTypes = {
  SHOW_ALERT: `${NAME}/SHOW_ALERT`,
  USER_LOGGED_IN: `${NAME}/USER_LOGGED_IN`,
  SET_ENTITY: `${NAME}/SET_ENTITY`,
  CLEAR_ALL: `${NAME}/CLEAR_ALL`,
  SET_USER_ROLE: `${NAME}/SET_USER_ROLE`,
}

const actions = {
  showAlert: ({ error, message, variant, logOnly = false }) => ({
    type: actionTypes.SHOW_ALERT,
    payload: { error, message, variant, logOnly },
  }),
  userLogged: ({ userName }) => ({
    type: actionTypes.USER_LOGGED_IN,
    payload: { userName },
  }),
  setEntity: (entity) => ({
    type: actionTypes.SET_ENTITY,
    payload: { entity },
  }),
  clearAll: () => ({
    type: actionTypes.CLEAR_ALL,
  }),
  setUserRole: (userRole) => ({
    type: actionTypes.SET_USER_ROLE,
    payload: { userRole },
  }),
}

const initialState = {
  entity: null,
  userRole: null,
}

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionTypes.SET_ENTITY:
      return { ...state, entity: payload.entity }
    case actionTypes.SET_USER_ROLE:
      return { ...state, userRole: payload.userRole }
    default:
      return state
  }
}

const getEntity = (state) => state[NAME].entity
const getUserRole = (state) => state[NAME].userRole

const selectors = { getEntity, getUserRole }

const ui = {
  namespace: NAME,
  initialState,
  actionTypes,
  actions,
  reducer,
  selectors,
}

export default ui
