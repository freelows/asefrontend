import { createSelector } from 'reselect'

const STATS_OWNER = 'system'

const NAME = 'favorites'

const actionTypes = {
  RESET: `${NAME}/RESET`,
  FETCHING: `${NAME}/FETCHING`,
  FETCHED: `${NAME}/FETCHED`,
  FETCH: `${NAME}/FETCH`,
  SET: `${NAME}/SET`,
  SELECT: `${NAME}/SELECT`,
  SET_SELECTED: `${NAME}/SET_SELECTED`,
  UPDATE: `${NAME}/UPDATE`,
  UPDATE_COUNTER: `${NAME}/UPDATE_COUNTER`,
  REMOVE: `${NAME}/REMOVE`,
  SAVE: `${NAME}/SAVE`
}

const actions = {
  fetching: () => ({
    type: actionTypes.FETCHING
  }),
  fetched: () => ({
    type: actionTypes.FETCHED
  }),
  fetch: ({ owner }) => ({
    type: actionTypes.FETCH,
    payload: { params: { owner } },
    meta: { feature: NAME }
  }),
  select: id => ({
    type: actionTypes.SELECT,
    payload: { id }
  }),
  set: ({ items, normalizeKey }) => ({
    type: actionTypes.SET,
    payload: items,
    meta: { normalizeKey, feature: NAME }
  }),
  setSelected: id => ({
    type: actionTypes.SET_SELECTED,
    payload: { id }
  }),
  reset: () => ({
    type: actionTypes.RESET
  }),
  save: ({ name, description, owner, createdBy }) => ({
    type: actionTypes.SAVE,
    payload: { name, description, owner, createdBy }
  })
}

export const initialState = {
  fetching: false,
  items: null,
  selectedItemId: null
}

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionTypes.RESET:
      return initialState

    case actionTypes.FETCHING:
      return { ...state, fetching: true }

    case actionTypes.FETCHED:
      return { ...state, fetching: false }

    case actionTypes.SET:
      return { ...state, items: payload }

    case actionTypes.SET_SELECTED:
      return { ...state, selectedItemId: payload.id }

    default:
      return state
  }
}

const getFetching = state => state[NAME].fetching
const getSelectedItemId = state => state[NAME].selectedItemId
const getItems = state => state[NAME].items
const getSelectedItem = state =>
  state[NAME].selectedItemId && state[NAME].items[state[NAME].selectedItemId]

const getAll = createSelector([getItems], items =>
  items ? Object.values(items) : []
)

// TODO: ADjust filter condition once the new api is ready
const getMyFavorites = createSelector(
  [getAll],
  favorites => favorites
  //Should we build the functionality to separate favorites with saved filters?
  //favorites =>
  //favorites.filter(favorite => favorite.createdBy !== STATS_OWNER).slice(0, 3)
)
const getStats = createSelector([getAll], favorites =>
  favorites.filter(favorite => favorite.createdBy === STATS_OWNER)
)

const selectors = {
  getAll,
  getSelectedItemId,
  getSelectedItem,
  getFetching,
  getMyFavorites,
  getStats
}

const favorites = {
  namespace: NAME,
  initialState,
  actionTypes,
  actions,
  reducer,
  selectors
}

export default favorites
