import { isNumber } from 'utils/dataTypes'

const appPrefix = 'lmkt'

export const userSettingsKey = 'user.settings'

export const setKey = (key, value) => {
  if (!key) return
  localStorage.setItem(
    `${appPrefix}.${key}`,
    !isNumber(value) ? JSON.stringify(value) : value
  )
}

export const getKey = (key) => {
  if (!key) return
  return JSON.parse(localStorage.getItem(`${appPrefix}.${key}`))
}

const checkLocalStorage = () => {
  try {
    const testSetting = '__test_setting_'
    localStorage.setItem(testSetting, testSetting)
    localStorage.removeItem(testSetting)
    return true
  } catch (e) {
    return false
  }
}

export const isLocalStorageAvailable = checkLocalStorage()
